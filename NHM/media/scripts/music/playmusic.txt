module NHM
{
	imports	{Base}	

	recipe Play Guitar 1
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarfast,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_Mental,
		Time:5640.0,
	}
	
	recipe Play Guitar 2
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarfast,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_LonelyDay,
		Time:10387.0,
	}

	recipe Play Guitar 3
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarfast,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_Aerials,
		Time:11092.0,
	}

	recipe Play Guitar 4
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarmedium,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_28days,
		Time:8084.0,
	}

	recipe Play Guitar 5
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarslow,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_Mutter,
		Time:10951.0,
	}

	recipe Play Guitar 6
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarmedium,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_Pirates,
		Time:4230.0,
	}

	recipe Play Guitar 7
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarmedium,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_BuildHome-T,
		Time:6392.0,
	}

	recipe Play Guitar 8
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarmedium,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_Fightclub,
		Time:5405.0,
	}

	recipe Play Guitar 9
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarmedium,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_MadWorld,
		Time:4136.0,
	}

	recipe Play Guitar 10
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarmedium,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_Inception,
		Time:14382.0,
	}

	recipe Play Guitar 11
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarmedium,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_Kookooshka,
		Time:11139.0,
	}

	recipe Play Guitar 12
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarmedium,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_Splin-T,
		Time:2773.0,
	}

	recipe Play Guitar 13
	{
		GuitarAcoustic,

		Result:GuitarAcoustic,
		
		AnimNode:playguitarmedium,
		Prop2:GuitarAcousticAnim,
		Sound:Guitar_Sting,
		Time:8601.0,
	}

	recipe Play Flute 
	{
		Flute,

		Result:Flute,
		
		AnimNode:playflute,
		Prop2:FluteAnim,
		Sound:Flute_Titanic,
		Time:2162.0,
	}
}