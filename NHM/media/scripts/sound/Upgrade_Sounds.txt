module Base
{
    sound UpgradeWeapon
	{
		category = Player,
		clip
		{
			file = media/sound/addWeaponUpgrade.ogg,
		}
	}
    sound RemoveUpgrade
	{
		category = Player,
		clip
		{
			file = media/sound/removeWeaponUpgrade.ogg,
		}
	}
}