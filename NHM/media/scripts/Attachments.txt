module Base
{
	model FemaleBody
	{
		mesh = Skinned/FemaleBody,

		attachment Holster_Vanila
		{
			offset = 0.0860 0.0950 -0.0050,
			rotate = 179.0000 2.0000 91.0000,
			bone = Bip01_Pelvis,
		}

		attachment Knife_Sheath_Leg
		{
			offset = -0.0490 0.0600 -0.0080,
			rotate = -13.0000 0.0000 -91.0000,
			bone = Bip01_R_Thigh,
		}

		attachment Tactical_Holster
		{
			offset = 0.0020 -0.0640 -0.0300,
			rotate = 180.0000 0.0000 -91.0000,
			bone = Bip01_L_Thigh,
		}

		attachment Sword_Sheath
		{
			offset = -0.0470 -0.1700 0.2000,
			rotate = -57.0000 -1.0000 -3.0000,
			bone = Bip01_BackPack,
		}
	}
	model MaleBody
	{
		mesh = Skinned/MaleBody,

		attachment Holster_Vanila
		{
			offset = 0.0860 0.0950 -0.0050,
			rotate = 179.0000 2.0000 91.0000,
			bone = Bip01_Pelvis,
		}

		attachment Knife_Sheath_Leg
		{
			offset = -0.0490 0.0600 -0.0080,
			rotate = -13.0000 0.0000 -91.0000,
			bone = Bip01_R_Thigh,
		}

		attachment Tactical_Holster
		{
			offset = -0.0080 -0.0640 -0.0350,
			rotate = 180.0000 0.0000 -91.0000,
			bone = Bip01_L_Thigh,
		}

		attachment Sword_Sheath
		{
			offset = -0.0110 -0.1700 0.1990,
			rotate = -57.0000 -1.0000 -3.0000,
			bone = Bip01_BackPack,
		}
	}
}
