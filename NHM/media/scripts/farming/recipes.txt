module NHFarming {
    imports {
        Base
    }

    recipe Open Packet of Tobacco Seeds {
        TobaccoBagSeed,

        Result:TobaccoSeeds=50,
        Time:20.0,
        Category:Farming,
        Sound:OpenSeedPacket,
    }

    recipe Put Tobacco Seeds in Packet {
        TobaccoSeeds=50,

        Result:TobaccoBagSeed,
        Time:10.0,
        Category:Farming,
    }

    recipe Open Packet of Corn Seeds {
        CornBagSeed,

        Result:CornSeeds=50,
        Time:20.0,
        Category:Farming,
        Sound:OpenSeedPacket,
    }

    recipe Put Corn Seeds in Packet {
        CornSeeds=50,

        Result:CornBagSeed,
        Time:10.0,
        Category:Farming,
    }

    recipe Open Packet of Cannabis Seeds {
        CannabisBagSeed,

        Result:CannabisSeeds=50,
        Time:20.0,
        Category:Farming,
        Sound:OpenSeedPacket,
    }

    recipe Put Cannabis Seeds in Packet {
        CannabisSeeds=50,

        Result:CannabisBagSeed,
        Time:10.0,
        Category:Farming,
    }

    recipe Open Packet of Rice Seeds {
        RiceBagSeed,

        Result:RiceSeeds=50,
        Time:20.0,
        Category:Farming,
        Sound:OpenSeedPacket,
    }

    recipe Put Rice Seeds in Packet {
        RiceSeeds=50,

        Result:RiceBagSeed,
        Time:10.0,
        Category:Farming,
    }

    recipe Open Sack of Rice
    {
        SackProduce_Rice,

        Result:Rice=250,
        OnCreate:Recipe.OnCreate.OpenSackProduce,
        OnGiveXP:Recipe.OnGiveXP.None,
        Sound:PutItemInBag,
        Time:15.0,
        AllowRottenItem:true,
    }

    recipe Put Rice in Sack
    {
        Rice=250,
        EmptySandbag,

        Result:SackProduce_Rice,
        OnGiveXP:Recipe.OnGiveXP.None,
        Sound:PutItemInBag,
        Time:15.0,
        AllowRottenItem:true,
    }



}