module Base
{		
	
	item Shield1
	{
	    DisplayCategory = Shield,
		Weight	=	8,
		Type	=	Drainable,
		KeepOnDeplete = true,
		UseWhileEquipped	=	FALSE,
		UseDelta	=	0.1,
		DisplayName	=	Shield1,
		Icon	=	Shield1,
		Tooltip = Tooltip_Shield1,
		StaticModel = Shield1,
		WorldStaticModel = Shield1,
        secondaryAnimMask = HoldingTorchLeft,
	}
	
	item Shield2
	{
	    DisplayCategory = Shield,
		Weight	=	8,
		Type	=	Drainable,
		KeepOnDeplete = true,
		UseWhileEquipped	=	FALSE,
		UseDelta	=	0.1,
		DisplayName	=	Shield2,
		Icon	=	Shield2,
		Tooltip = Tooltip_Shield2,
		StaticModel = Shield2,
		WorldStaticModel = Shield2,
        secondaryAnimMask = HoldingTorchLeft,
	}
	
	item Shield2Shark
	{
	    DisplayCategory = Shield,
		Weight	=	8,
		Type	=	Drainable,
		KeepOnDeplete = true,
		UseWhileEquipped	=	FALSE,
		UseDelta	=	0.1,
		DisplayName	=	Shield2Shark,
		Icon	=	Shield2,
		Tooltip = Tooltip_Shield2,
		StaticModel = Shield2Shark,
		WorldStaticModel = Shield2Shark,
        secondaryAnimMask = HoldingTorchLeft,
	}

    model Shield1
	{
		mesh = Shield1,
		texture = Shield1,
	}
	
    model Shield2
	{
		mesh = Shield2,
		texture = Shield2,
	}
	
    model Shield2Shark
	{
		mesh = Shield2,
		texture = Shield2Shark,
	}
}