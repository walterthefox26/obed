local json = require("json")

local playerDataServer = {}

ClientCommandRead = {}
ServerSyncXP = {}

function ServerSyncXP:Load()
    local fileReaderObj = getFileReader("playerDataServer.json", true);
    local text = "";
    local line = fileReaderObj:readLine()
    while line do
        text = text..line
        line = fileReaderObj:readLine()
    end
    fileReaderObj:close()

    if text and text ~= "" then

        local playerData = {}
        local playerDataJson = json.parse(text)

        for key, val in pairs(playerDataJson) do
            local playerDataLocal = {}
            for key2, val2 in pairs(playerDataJson[key]) do
                table.insert(playerDataLocal, val2)
            end
            table.remove(playerDataLocal, 1)
            playerData[key] = playerDataLocal
        end

        for key, val in pairs(playerData) do
            print('Key: ' ..  key)
            for key2, val2 in pairs(playerData[key]) do
                print('Value: ' .. val2)
            end
        end

        return playerData
    else
        return {}
    end
end

function ServerSyncXP:init()
    print("[Server XP System] Initializing mod..")
    playerDataServer = ServerSyncXP:Load()
    print("[Server XP System] Mod initialization completed!")
end

ClientCommandRead.SaveXP = function(args, player)
    local SteamID = args.SteamID
    local playerData = args.PlayerData
    print("[Server SaveXP Command]:" .. tostring(SteamID) .. " - " .. "saved!")
    for i, v in ipairs(playerData) do
        print("[Server SaveXP PlayerData]:" .. tostring(i) .. " - " .. tostring(v) .. " received!")
    end

    playerDataServer[SteamID] = playerData

    local fileWriterObj = getFileWriter("playerDataServer.json", true, false);
    local jsonString = json.stringify(playerDataServer, false)
    fileWriterObj:write(jsonString);
    fileWriterObj:close();

end

ClientCommandRead.DropXP = function(args, player)
    local SteamID = args.SteamID
    print("[Server DropXP Command]:" .. tostring(SteamID) .. " - " .. "dropped!")
    playerDataServer[SteamID] = {}

    local fileWriterObj = getFileWriter("playerDataServer.json", true, false);
    local jsonString = json.stringify(playerDataServer, false)
    fileWriterObj:write(jsonString);
    fileWriterObj:close();
end

ClientCommandRead.RequestXP = function(args, player)
    local SteamID = args.SteamID
    local playerData = playerDataServer[SteamID]
    if playerData == nil then
        playerData = {}
    end
    print("[Server RequestXP Command]:" .. tostring(SteamID) .. " - " .. "requested!")
    if #playerData ~= 0 then
        for i, v in ipairs(playerData) do
            print("[Server RequestXP PlayerData]:" .. tostring(i) .. " - " .. tostring(v) .. " requested!")
        end
    end
    local data = {}
    data.PlayerData = playerData

    sendServerCommand(player, "server", "ReceiveXP", data)
end

function ClientCommandRead:onClientCommand(command, player, args)
    if ClientCommandRead[command] then
        ClientCommandRead[command](args, player)
    end
end

if isServer() then
    Events.OnServerStarted.Add(ServerSyncXP.init)
    Events.OnClientCommand.Add(ClientCommandRead.onClientCommand)
end