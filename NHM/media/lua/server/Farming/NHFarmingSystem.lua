if ProceduralDistributions ~= nil then
    if isClient() then return end
    end
    
    require "Farming/SFarmingSystem"
    require "Farming/SPlantGlobalObject"
    require "Map/CGlobalObject"
    
    -- HERE WE ADD NEW PLANTS GROWING LOGIC TO THE MAIN GROW LOGIC
    
    function SFarmingSystem:growPlant(luaObject, nextGrowing, updateNbOfGrow)
        if(luaObject.state == "seeded") then
            local new = luaObject.nbOfGrow <= 0
    
            if(luaObject.typeOfSeed == "Carrots") then
                luaObject = farming_vegetableconf.growCarrots(luaObject, nextGrowing, updateNbOfGrow)
            elseif(luaObject.typeOfSeed == "Broccoli") then
                luaObject = farming_vegetableconf.growBroccoli(luaObject, nextGrowing, updateNbOfGrow)
            elseif(luaObject.typeOfSeed == "Strawberry plant") then
                luaObject = farming_vegetableconf.growStrewberries(luaObject, nextGrowing, updateNbOfGrow)
            elseif(luaObject.typeOfSeed == "Radishes") then
                luaObject = farming_vegetableconf.growRedRadish(luaObject, nextGrowing, updateNbOfGrow)
            elseif(luaObject.typeOfSeed == "Tomato") then
                luaObject = farming_vegetableconf.growTomato(luaObject, nextGrowing, updateNbOfGrow)
            elseif(luaObject.typeOfSeed == "Potatoes") then
                luaObject = farming_vegetableconf.growPotato(luaObject, nextGrowing, updateNbOfGrow)
            elseif(luaObject.typeOfSeed == "Cabbages") then
                luaObject = farming_vegetableconf.growCabbage(luaObject, nextGrowing, updateNbOfGrow)
            elseif (luaObject.typeOfSeed == "Tobacco") then
                luaObject = FarmingConfig.growTobacco(luaObject, nextGrowing, updateNbOfGrow)
            elseif (luaObject.typeOfSeed == "Corn") then
                luaObject = FarmingConfig.growCorn(luaObject, nextGrowing, updateNbOfGrow)
            elseif (luaObject.typeOfSeed == "Cannabis") then
                luaObject = FarmingConfig.growCannabis(luaObject, nextGrowing, updateNbOfGrow)
            elseif (luaObject.typeOfSeed == "Rice") then
                luaObject = FarmingConfig.growRice(luaObject, nextGrowing, updateNbOfGrow)
            end

            if not new and luaObject.nbOfGrow > 0 then
                self:diseaseThis(luaObject, true)
            end
            luaObject.nbOfGrow = luaObject.nbOfGrow + 1
        end
    end

    --HERE WE SET ROTTEN TEXTURE OF THE PLANT
    
    function SPlantGlobalObject:rottenThis()
        local texture = nil
        if self.typeOfSeed == "Carrots" then
            texture = "vegetation_farming_01_13"
        elseif self.typeOfSeed == "Broccoli" then
            texture = "vegetation_farming_01_23"
        elseif self.typeOfSeed == "Strawberry plant" then
            texture = "vegetation_farming_01_63"
        elseif self.typeOfSeed == "Radishes" then
            texture = "vegetation_farming_01_39"
        elseif self.typeOfSeed == "Tomato" then
            texture = "vegetation_farming_01_71"
        elseif self.typeOfSeed == "Potatoes" then
            texture = "vegetation_farming_01_47"
        elseif self.typeOfSeed == "Cabbages" then
            texture = "vegetation_farming_01_31"
        elseif self.typeOfSeed == "Tobacco" then
            texture = "vegetation_farming_01_23"
        elseif self.typeOfSeed == "Corn" then
            texture = "vegetation_farming_01_23"
        elseif self.typeOfSeed == "Cannabis" then
            texture = "vegetation_farming_01_23"
        elseif self.typeOfSeed == "Rice" then
            texture = "vegetation_farming_01_23"
        end
        if texture ~= nil then
            self:setSpriteName(texture)
        end
        self.state = "rotten"
        self:setObjectName(farming_vegetableconf.getObjectName(self))
        self:deadPlant()
    end

    --HERE WE CAN CHANGE THE PLANT GROWING CYCLE: for example, set the ability to grow again (as strawberrie)

    function SFarmingSystem:harvest(luaObject, player)
        local props = farming_vegetableconf.props[luaObject.typeOfSeed]
        local numberOfVeg = getVegetablesNumber(props.minVeg, props.maxVeg, props.minVegAutorized, props.maxVegAutorized, luaObject)
        if player then
            player:sendObjectChange('addItemOfType', { type = props.vegetableName, count = numberOfVeg })
        end
    
        if luaObject.hasSeed and player then
            player:sendObjectChange('addItemOfType', { type = props.seedName, count = (props.seedPerVeg * numberOfVeg) })
        end
    
        luaObject.hasVegetable = false
        luaObject.hasSeed = false
    
        -- the strawberrie don't disapear, it goes on phase 2 again
        if luaObject.typeOfSeed == "Strawberry plant" or luaObject.typeOfSeed == "Tobacco" then
            luaObject.nbOfGrow = 1
            luaObject.fertilizer = 0;
            self:growPlant(luaObject, nil, true)
            luaObject:saveData()
        else
            self:removePlant(luaObject)
        end
    end

    --HERE WE CHANGE HEALTH OF THE PLANTS

    function SFarmingSystem:changeHealth()
        for i=1,self:getLuaObjectCount() do
            local luaObject = self:getLuaObjectByIndex(i)
            local currentSquare = luaObject:getSquare();
            local currentTemp;
            local MIN_TEMP = 2;
            local GOOD_TEMP = 15;
            local MAX_TEMP = 32;
            local climateManager = getClimateManager();
            if luaObject.exterior then
                currentTemp = climateManager:getTemperature();
            else
                currentTemp = climateManager:getAirTemperatureForSquare(currentSquare);
            end
            if currentTemp >= GOOD_TEMP and currentTemp <= MAX_TEMP then
                luaObject.health = luaObject.health + 1;
            elseif currentTemp >= MIN_TEMP and currentTemp < GOOD_TEMP then
                luaObject.health = luaObject.health + 0.4;
            else
                luaObject.health = luaObject.health - 5;
            end

            -- change with water
            local water = farming_vegetableconf.calcWater(luaObject.waterNeeded, luaObject.waterLvl)
            local waterMax = farming_vegetableconf.calcWater(luaObject.waterLvl, luaObject.waterNeededMax)
            if water >= 0 and waterMax >= 0 then
                luaObject.health = luaObject.health + 0.4
            elseif water == -1 then -- we low health by 0.2
                luaObject.health = luaObject.health - 0.2
            elseif water == -2 then -- low health by 0.5
                luaObject.health = luaObject.health - 0.5
            elseif waterMax == -1 and luaObject.health > 20  then
                luaObject.health = luaObject.health - 0.2
            elseif waterMax == -2 and luaObject.health > 20  then
                luaObject.health = luaObject.health - 0.5
            end
        end
    end