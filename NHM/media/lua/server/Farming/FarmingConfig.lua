require "Farming/farming_vegetableconf"

FarmingConfig = {};

--HERE WE NEED TO ADD NEW GROWING LOGIC FOR NEW PLANTS

FarmingConfig.growTobacco = function(planting, nextGrowing, updateNbOfGrow)
	local stageOfGrow = planting.nbOfGrow;

	local water = farming_vegetableconf.calcWater(planting.waterNeeded, planting.waterLvl);
	local diseaseLvl = farming_vegetableconf.calcDisease(planting.mildewLvl);
	if(stageOfGrow <= 0) then
		stageOfGrow = 0;
		planting.nbOfGrow = 0;

		planting = growNext(planting, farming_vegetableconf.getSpriteName(planting), farming_vegetableconf.getObjectName(planting), nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);

		planting.waterNeeded = 90;
	elseif (stageOfGrow <= 4) then
		if(water >= 0 and diseaseLvl >= 0) then

			planting = growNext(planting, farming_vegetableconf.getSpriteName(planting), farming_vegetableconf.getObjectName(planting), nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);

			planting.waterNeeded = farming_vegetableconf.props[planting.typeOfSeed].waterLvl;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (stageOfGrow == 5) then
		if(water >= 0 and diseaseLvl >= 0) then
			planting.nextGrowing = calcNextGrowing(nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);
			planting:setObjectName(farming_vegetableconf.getObjectName(planting))
			planting:setSpriteName(farming_vegetableconf.getSpriteName(planting))
			planting.hasVegetable = true;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (stageOfGrow == 6) then
		if(water >= 0 and diseaseLvl >= 0) then
			planting.nextGrowing = calcNextGrowing(nextGrowing, 168);
			planting:setObjectName(farming_vegetableconf.getObjectName(planting))
			planting:setSpriteName(farming_vegetableconf.getSpriteName(planting))
			planting.hasVegetable = true;
			planting.hasSeed = true;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (planting.state ~= "rotten") then
		planting:rottenThis()
	end
	return planting;
end

FarmingConfig.growCorn = function(planting, nextGrowing, updateNbOfGrow)
	local stageOfGrow = planting.nbOfGrow;

	local water = farming_vegetableconf.calcWater(planting.waterNeeded, planting.waterLvl);
	local diseaseLvl = farming_vegetableconf.calcDisease(planting.mildewLvl);
	if(stageOfGrow <= 0) then
		stageOfGrow = 0;
		planting.nbOfGrow = 0;

		planting = growNext(planting, farming_vegetableconf.getSpriteName(planting), farming_vegetableconf.getObjectName(planting), nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);

		planting.waterNeeded = 90;
	elseif (stageOfGrow <= 4) then
		if(water >= 0 and diseaseLvl >= 0) then

			planting = growNext(planting, farming_vegetableconf.getSpriteName(planting), farming_vegetableconf.getObjectName(planting), nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);

			planting.waterNeeded = farming_vegetableconf.props[planting.typeOfSeed].waterLvl;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (stageOfGrow == 5) then
		if(water >= 0 and diseaseLvl >= 0) then
			planting.nextGrowing = calcNextGrowing(nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);
			planting:setObjectName(farming_vegetableconf.getObjectName(planting))
			planting:setSpriteName(farming_vegetableconf.getSpriteName(planting))
			planting.hasVegetable = true;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (stageOfGrow == 6) then
		if(water >= 0 and diseaseLvl >= 0) then
			planting.nextGrowing = calcNextGrowing(nextGrowing, 168);
			planting:setObjectName(farming_vegetableconf.getObjectName(planting))
			planting:setSpriteName(farming_vegetableconf.getSpriteName(planting))
			planting.hasVegetable = true;
			planting.hasSeed = true;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (planting.state ~= "rotten") then
		planting:rottenThis()
	end
	return planting;
end

FarmingConfig.growCannabis = function(planting, nextGrowing, updateNbOfGrow)
	local stageOfGrow = planting.nbOfGrow;

	local water = farming_vegetableconf.calcWater(planting.waterNeeded, planting.waterLvl);
	local diseaseLvl = farming_vegetableconf.calcDisease(planting.mildewLvl);
	if(stageOfGrow <= 0) then
		stageOfGrow = 0;
		planting.nbOfGrow = 0;

		planting = growNext(planting, farming_vegetableconf.getSpriteName(planting), farming_vegetableconf.getObjectName(planting), nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);

		planting.waterNeeded = 90;
	elseif (stageOfGrow <= 4) then
		if(water >= 0 and diseaseLvl >= 0) then

			planting = growNext(planting, farming_vegetableconf.getSpriteName(planting), farming_vegetableconf.getObjectName(planting), nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);

			planting.waterNeeded = farming_vegetableconf.props[planting.typeOfSeed].waterLvl;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (stageOfGrow == 5) then
		if(water >= 0 and diseaseLvl >= 0) then
			planting.nextGrowing = calcNextGrowing(nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);
			planting:setObjectName(farming_vegetableconf.getObjectName(planting))
			planting:setSpriteName(farming_vegetableconf.getSpriteName(planting))
			planting.hasVegetable = true;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (stageOfGrow == 6) then
		if(water >= 0 and diseaseLvl >= 0) then
			planting.nextGrowing = calcNextGrowing(nextGrowing, 168);
			planting:setObjectName(farming_vegetableconf.getObjectName(planting))
			planting:setSpriteName(farming_vegetableconf.getSpriteName(planting))
			planting.hasVegetable = true;
			planting.hasSeed = true;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (planting.state ~= "rotten") then
		planting:rottenThis()
	end
	return planting;
end

FarmingConfig.growRice = function(planting, nextGrowing, updateNbOfGrow)
	local stageOfGrow = planting.nbOfGrow;

	local water = farming_vegetableconf.calcWater(planting.waterNeeded, planting.waterLvl);
	local diseaseLvl = farming_vegetableconf.calcDisease(planting.mildewLvl);
	if(stageOfGrow <= 0) then
		stageOfGrow = 0;
		planting.nbOfGrow = 0;

		planting = growNext(planting, farming_vegetableconf.getSpriteName(planting), farming_vegetableconf.getObjectName(planting), nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);

		planting.waterNeeded = 90;
	elseif (stageOfGrow <= 4) then
		if(water >= 0 and diseaseLvl >= 0) then

			planting = growNext(planting, farming_vegetableconf.getSpriteName(planting), farming_vegetableconf.getObjectName(planting), nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);

			planting.waterNeeded = farming_vegetableconf.props[planting.typeOfSeed].waterLvl;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (stageOfGrow == 5) then
		if(water >= 0 and diseaseLvl >= 0) then
			planting.nextGrowing = calcNextGrowing(nextGrowing, farming_vegetableconf.props[planting.typeOfSeed].timeToGrow + water + diseaseLvl);
			planting:setObjectName(farming_vegetableconf.getObjectName(planting))
			planting:setSpriteName(farming_vegetableconf.getSpriteName(planting))
			planting.hasVegetable = true;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (stageOfGrow == 6) then
		if(water >= 0 and diseaseLvl >= 0) then
			planting.nextGrowing = calcNextGrowing(nextGrowing, 168);
			planting:setObjectName(farming_vegetableconf.getObjectName(planting))
			planting:setSpriteName(farming_vegetableconf.getSpriteName(planting))
			planting.hasVegetable = true;
			planting.hasSeed = true;
		else
			badPlant(water, nil, diseaseLvl, planting, nextGrowing, updateNbOfGrow);
		end
	elseif (planting.state ~= "rotten") then
		planting:rottenThis()
	end
	return planting;
end

--HERE WE NEED TO SET UP PROPERTIES FOR NEW PLANTS

farming_vegetableconf.icons["Tobacco"] = "media/textures/Farming/vegetableIcons/item_Avocado.png";

farming_vegetableconf.props["Tobacco"] = {};
farming_vegetableconf.props["Tobacco"].seedsRequired = 4;
farming_vegetableconf.props["Tobacco"].texture = "vegetation_farming_01_31";
farming_vegetableconf.props["Tobacco"].waterLvl = 95;
farming_vegetableconf.props["Tobacco"].timeToGrow = ZombRand(56, 62);
farming_vegetableconf.props["Tobacco"].vegetableName = "NHFarming.Tobacco";
farming_vegetableconf.props["Tobacco"].seedName = "NHFarming.TobaccoSeeds";
farming_vegetableconf.props["Tobacco"].growCode = "FarmingConfig.growTobacco";
farming_vegetableconf.props["Tobacco"].seedPerVeg = 1;
farming_vegetableconf.props["Tobacco"].minVeg = 4;
farming_vegetableconf.props["Tobacco"].maxVeg = 5;
farming_vegetableconf.props["Tobacco"].minVegAutorized = 6;
farming_vegetableconf.props["Tobacco"].maxVegAutorized = 10;

farming_vegetableconf.icons["Corn"] = "media/textures/Farming/vegetableIcons/item_Avocado.png";

farming_vegetableconf.props["Corn"] = {};
farming_vegetableconf.props["Corn"].seedsRequired = 4;
farming_vegetableconf.props["Corn"].texture = "vegetation_farming_01_31";
farming_vegetableconf.props["Corn"].waterLvl = 95;
farming_vegetableconf.props["Corn"].timeToGrow = ZombRand(56, 62);
farming_vegetableconf.props["Corn"].vegetableName = "NHFarming.Corn";
farming_vegetableconf.props["Corn"].seedName = "NHFarming.CornSeeds";
farming_vegetableconf.props["Corn"].growCode = "FarmingConfig.growCorn";
farming_vegetableconf.props["Corn"].seedPerVeg = 1;
farming_vegetableconf.props["Corn"].minVeg = 4;
farming_vegetableconf.props["Corn"].maxVeg = 5;
farming_vegetableconf.props["Corn"].minVegAutorized = 6;
farming_vegetableconf.props["Corn"].maxVegAutorized = 10;

farming_vegetableconf.icons["Cannabis"] = "media/textures/Farming/vegetableIcons/item_Avocado.png";

farming_vegetableconf.props["Cannabis"] = {};
farming_vegetableconf.props["Cannabis"].seedsRequired = 4;
farming_vegetableconf.props["Cannabis"].texture = "vegetation_farming_01_31";
farming_vegetableconf.props["Cannabis"].waterLvl = 95;
farming_vegetableconf.props["Cannabis"].timeToGrow = ZombRand(56, 62);
farming_vegetableconf.props["Cannabis"].vegetableName = "NHFarming.Cannabis";
farming_vegetableconf.props["Cannabis"].seedName = "NHFarming.CannabisSeeds";
farming_vegetableconf.props["Cannabis"].growCode = "FarmingConfig.growCannabis";
farming_vegetableconf.props["Cannabis"].seedPerVeg = 1;
farming_vegetableconf.props["Cannabis"].minVeg = 4;
farming_vegetableconf.props["Cannabis"].maxVeg = 5;
farming_vegetableconf.props["Cannabis"].minVegAutorized = 6;
farming_vegetableconf.props["Cannabis"].maxVegAutorized = 10;

farming_vegetableconf.icons["Rice"] = "media/textures/Farming/vegetableIcons/item_Avocado.png";

farming_vegetableconf.props["Rice"] = {};
farming_vegetableconf.props["Rice"].seedsRequired = 4;
farming_vegetableconf.props["Rice"].texture = "vegetation_farming_01_31";
farming_vegetableconf.props["Rice"].waterLvl = 95;
farming_vegetableconf.props["Rice"].timeToGrow = ZombRand(56, 62);
farming_vegetableconf.props["Rice"].vegetableName = "NHFarming.Rice";
farming_vegetableconf.props["Rice"].seedName = "NHFarming.RiceSeeds";
farming_vegetableconf.props["Rice"].growCode = "FarmingConfig.growRice";
farming_vegetableconf.props["Rice"].seedPerVeg = 1;
farming_vegetableconf.props["Rice"].minVeg = 4;
farming_vegetableconf.props["Rice"].maxVeg = 5;
farming_vegetableconf.props["Rice"].minVegAutorized = 6;
farming_vegetableconf.props["Rice"].maxVegAutorized = 10;

farming_vegetableconf.sprite["Tobacco"] = {
	"vegetation_farming_01_24",
	"vegetation_farming_01_25",
	"vegetation_farming_01_26",
	"vegetation_farming_01_27",
	"vegetation_farming_01_28",
	"vegetation_farming_01_30",
	"vegetation_farming_01_29",
	"vegetation_farming_01_31"
}

farming_vegetableconf.sprite["Corn"] = {
	"vegetation_farming_01_24",
	"vegetation_farming_01_25",
	"vegetation_farming_01_26",
	"vegetation_farming_01_27",
	"vegetation_farming_01_28",
	"vegetation_farming_01_30",
	"vegetation_farming_01_29",
	"vegetation_farming_01_31"
}

farming_vegetableconf.sprite["Cannabis"] = {
	"vegetation_farming_01_24",
	"vegetation_farming_01_25",
	"vegetation_farming_01_26",
	"vegetation_farming_01_27",
	"vegetation_farming_01_28",
	"vegetation_farming_01_30",
	"vegetation_farming_01_29",
	"vegetation_farming_01_31"
}

farming_vegetableconf.sprite["Rice"] = {
	"vegetation_farming_01_24",
	"vegetation_farming_01_25",
	"vegetation_farming_01_26",
	"vegetation_farming_01_27",
	"vegetation_farming_01_28",
	"vegetation_farming_01_30",
	"vegetation_farming_01_29",
	"vegetation_farming_01_31"
}