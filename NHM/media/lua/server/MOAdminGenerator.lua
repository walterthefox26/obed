local adminGeneratorList = {}

local stateMap, UpdateGenSprite = unpack(require 'AdminGeneratorTweaks')

local function ReplaceExistingObject(object, fuel, condition)
	local cell = getWorld():getCell()
	local sq = object:getSquare()
	
	local item = InventoryItemFactory.CreateItem("NHM.Generator")
	if not item then
		return
	end
	
	item:setCondition(condition)
	local itemModData = item:getModData()
	itemModData.fuel = fuel
	
	sq:transmitRemoveItemFromSquare(object)
	local generatorObj = IsoGenerator.new(item, cell, square)
    generatorObj:setFuel(itemModData.fuel);
	generatorObj:setSprite(getSprite(stateMap.off))
	generatorObj:transmitCompleteItemToClients()
	return generatorObj
end

local function createAdminGenerator(generatorObj)
	generatorObj = instanceof(generatorObj, 'IsoGenerator') and generatorObj or ReplaceExistingObject(generatorObj, 100, 100)
	adminGeneratorList[generatorObj] = true
end

local function loadAdminGenerator(generatorObj)
	adminGeneratorList[generatorObj] = true
end

local function updateFuelAndConditionLevel(generatorObj)
    generatorObj:setCondition(100);
    generatorObj:setFuel(100);
	generatorObj:transmitModData()
end

Events.EveryTenMinutes.Add(function()
	for k,_ in pairs(adminGeneratorList) do
		if k:isActivated() then
			updateFuelAndConditionLevel(k);
            UpdateGenSprite(k);
		end
	end
end)

local PRIORITY = 5
for _,v in pairs(stateMap) do
	MapObjects.OnNewWithSprite(v, createAdminGenerator, PRIORITY)
	MapObjects.OnLoadWithSprite(v, loadAdminGenerator, PRIORITY)
end

Events.OnObjectAdded.Add(function(isoObject)
	if instanceof(isoObject, 'IsoGenerator')then
		createAdminGenerator(isoObject)
	end
end)

Events.OnObjectAboutToBeRemoved.Add(function(isoObject)
	adminGeneratorList[isoObject] = nil
end)