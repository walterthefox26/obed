require 'TimedActions/ISActivateGenerator'

local UpdateGenSprite = unpack(require 'AdminGeneratorTweaks')

ISActivateAdminGenerator = ISActivateGenerator:derive('ISActivateAdminGenerator')

function ISActivateAdminGenerator:isValid(...)
	return ISActivateGenerator.isValid(self, ...) and not self.activate
end

function ISActivateAdminGenerator:perform(...)
	if self.activate then
		return ISBaseTimedAction.perform(self, ...)
	end
	local resultAction = {ISActivateGenerator.perform(self, ...)}
	UpdateGenSprite(self.generator)
	return unpack(resultAction)
end