require 'TimedActions/ISUnequipAction'
require 'TimedActions/ISCraftAction'
require 'TimedActions/ISDropItemAction'
require 'TimedActions/ISDropWorldItemAction'
require 'TimedActions/ISInventoryTransferAction'

require 'ISUI/ISGeneratorInfoWindow'
require 'ISUI/ISWorldObjectContextMenu'

local stateMap = unpack(require 'AdminGeneratorTweaks')

local function transmitGenData(generatorObj, invItem)
	generatorObj:setSprite(getSprite(stateMap.off))
    generatorObj:transmitUpdatedSprite()
	generatorObj:transmitModData()
    print('Triggered');
	triggerEvent('OnObjectAdded', generatorObj) -- for single player
end

local function generatorObjPerform(oldPerform)
	return function(self, ...)
		local oldBehavior = {oldPerform(self, ...)}
		if self.item:getFullType() == "NHM.Generator" then
			local generatorObj = self.character:getCurrentSquare():getGenerator()
			if generatorObj then
				transmitGenData(generatorObj, self.item)
			end
		end
		return unpack(oldBehavior)
	end
end

ISUnequipAction.perform = generatorObjPerform(ISUnequipAction.perform)
ISDropItemAction.perform = generatorObjPerform(ISDropItemAction.perform)
ISDropWorldItemAction.perform = generatorObjPerform(ISDropWorldItemAction.perform)

local oldDropAction = ISCraftAction.addOrDropItem
function ISCraftAction:addOrDropItem(item, ...)
	local oldBehavior = {oldDropAction(self, item, ...)}
	if item:getFullType() == 'NHM.Generator' then
		local generatorObj = self.character:getCurrentSquare():getGenerator()
		if generatorObj then
			transmitGenData(generatorObj, item)
		end
	end
	return unpack(oldBehavior)
end

local oldTransferAction = ISInventoryTransferAction.transferItem
function ISInventoryTransferAction:transferItem(...)
	local isAdminGenObj = self.item:getFullType() == 'NHM.Generator'
	local isDropping = not self.destContainer or self.destContainer:getType() == 'floor'
	local sqOccupied = self.character:getCurrentSquare():getGenerator()
	if isAdminGenObj and isDropping and sqOccupied then
		return
	end
	local oldBehavior = {oldTransferAction(self, ...)}
	if isAdminGenObj and isDropping then
		local generatorObj = self.character:getCurrentSquare():getGenerator()
		if generatorObj then
			transmitGenData(generatorObj, item)
		end
	end
	return unpack(oldBehavior)
end

local generator
Events.OnPreFillWorldObjectContextMenu.Add(function(player, context, worldobjects)
local isAdminGen = _G.generator and (_G.generator:getSprite():getName():find('adm_gen_0_0'))
	if isAdminGen then
		generator = _G.generator
		_G.generator = nil
	end
end)

local function onActivateAdminGenerator(worldobjects, enable, generator, player)
	local playerObj = getSpecificPlayer(player)
	if luautils.walkAdj(playerObj, generator:getSquare()) then
		ISTimedActionQueue.add(ISActivateGenerator:new(player, generator, enable, 30))
	end
end

local function predicatePetrol(item)
	return (item:hasTag("Petrol") or item:getType() == "PetrolCan") and (item:getDrainableUsesInt() > 0)
end

Events.OnFillWorldObjectContextMenu.Add(function(player, context, worldobjects)
	if generator then
		local isConnected = generator:isConnected()
		local isOn = generator:isActivated()
		local playerObj = getSpecificPlayer(player)
		local playerInv = playerObj:getInventory()
		local petrolCan = playerInv:getFirstEvalRecurse(predicatePetrol);
		local option = context:addOption(getText("ContextMenu_GeneratorInfo"), worldobjects, ISWorldObjectContextMenu.onInfoGenerator, generator, player)
		if playerObj:DistToSquared(generator:getX() + 0.5, generator:getY() + 0.5) < 2 * 2 then
			local tooltip = ISWorldObjectContextMenu.addToolTip()
			tooltip:setName(getText('IGUI_Generator_TypeSteam'))
			tooltip.description = ISGeneratorInfoWindow.getRichText(generator, true)
			option.toolTip = tooltip
		end
		if generator:getFuel() < 100 then
			ISWorldObjectContextMenu.onAddFuelGenerator(worldobjects, petrolCan, generator, player, context)
		end
		if isConnected then
			local option = context:addOption(getText('ContextMenu_Turn_' .. (isOn and 'Off' or 'On')), worldobjects, 
			onActivateAdminGenerator,
			not isOn, generator, player)
		end
		if not isOn then
			local option = context:addOption(getText('ContextMenu_Generator' .. (isConnected and 'Unplug' or 'Plug')),
				worldobjects, ISWorldObjectContextMenu.onPlugGenerator, generator, player, not isConnected)
			local tooltip = ISWorldObjectContextMenu.addToolTip()
			tooltip.description = getText('ContextMenu_GeneratorPlugTT')
			option.toolTip = option.notAvailable and tooltip or nil
		end
	end
	generator = nil
end)