local playerData = {}
local Session = false

RespawnSystem = {}
ServerCommandRead = {}

RespawnSystem.RespawnListener = function(player)
    local tickCount = 0
    local function Respawn(player)
        if tickCount < 10 then
            tickCount = tickCount + 1
            return
        end

        local xp = player:getXp()

        for i = 0, PerkFactory.PerkList:size() - 1 do
            local perk = PerkFactory.PerkList:get(i)
            if tostring(perk) == "RepDoc" or tostring(perk) == "RepArmy" or tostring(perk) == "RepCrafts" then
                xp:AddXP(perk, playerData[i] * 4)
                print("[Local Respawn Event]:" .. tostring(perk) .. " - " .. tostring(playerData[i]) .. " added!")
            end
        end

        playerData = {}

        local args = {}
        args.SteamID = tostring(getPlayer():getSteamID())

        sendClientCommand(getPlayer(), "client", "DropXP", args)
        Events.OnPlayerUpdate.Remove(Respawn)
    end
    Events.OnPlayerUpdate.Add(Respawn)
    Events.OnCreatePlayer.Remove(RespawnSystem.RespawnListener)
end

RespawnSystem.Death = function(player)
    Session = true
    local xp = player:getXp()

    for i = 0, PerkFactory.PerkList:size() - 1 do
        local perk = PerkFactory.PerkList:get(i)
        playerData[i] = math.ceil(xp:getXP(perk))
        print("[Local Death Event]:" .. tostring(perk) .. " - " .. tostring(playerData[i]) .. " saved!")
    end

    local args = {}
    args.SteamID = tostring(getPlayer():getSteamID())
    args.PlayerData = playerData

    sendClientCommand(getPlayer(), "client", "SaveXP", args)
    Events.OnCreatePlayer.Add(RespawnSystem.RespawnListener)
end

RespawnSystem.ConnectionListener = function(player)
    local tickCount = 0

    local args = {}
    args.SteamID = tostring(getPlayer():getSteamID())

    sendClientCommand(getPlayer(), "client", "RequestXP", args)

    local function Respond(player)
        if Session == false then

            if tickCount < 25 then
                tickCount = tickCount + 1
                return
            end

            print("[Local Receive Event]: Data successfully received!")
            if #playerData ~= 0 then
                for i, v in ipairs(playerData) do
                    print("[Local Receive Event]:" .. tostring(i) .. " - " .. tostring(v) .. " received!")
                end
                local xp = player:getXp()
                for i = 0, PerkFactory.PerkList:size() - 1 do
                    local perk = PerkFactory.PerkList:get(i)
                    if tostring(perk) == "RepDoc" or tostring(perk) == "RepArmy" or tostring(perk) == "RepCrafts" then
                        xp:AddXP(perk, playerData[i] * 4)
                        print("[Local Receive Event]:" .. tostring(perk) .. " - " .. tostring(playerData[i]) .. " added!")
                    end
                end

                playerData = {}

                local data = {}
                data.SteamID = tostring(getPlayer():getSteamID())

                sendClientCommand(getPlayer(), "client", "DropXP", data)

            else
                print("[Local Receive Event]: No XP received from server!")
            end
            Events.EveryTenMinutes.Remove(RespawnSystem.ConnectionListener)
        else
            print("[Local Receive Event]: Session already exist!")
        end
        Events.OnPlayerUpdate.Remove(Respond)
    end
    Events.OnPlayerUpdate.Add(Respond)
end

ServerCommandRead.ReceiveXP = function(data)
    playerData = data.PlayerData
    if #playerData ~= 0 then
        for i, v in ipairs(playerData) do
            print("[Local Receive Event Handler]:" .. tostring(i) .. " - " .. tostring(v) .. " received!")
        end
    else
        print("[Local Receive Event Handler]: No XP received from server!")
    end
end

function ServerCommandRead:onServerCommand(command, args)
    if ServerCommandRead[command] then
        ServerCommandRead[command](args)
    end
end

if isClient() then
    Events.OnServerCommand.Add(ServerCommandRead.onServerCommand)
    Events.EveryTenMinutes.Add(RespawnSystem.ConnectionListener)
    Events.OnPlayerDeath.Add(RespawnSystem.Death)
end
