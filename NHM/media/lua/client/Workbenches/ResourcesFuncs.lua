require "TimedActions/ISBaseTimedAction"

local ebanie_cordinati = {x = nil, y = nil, z = nil}

--Функция для изменения энергии
function ChangeEnergyLevelWithSync(playerObj, level)
    local Consumption = level * 15 * 4
    if playerObj:HasTrait("FastLearner") then
        Consumption = Consumption / 1.3
    end
    if playerObj:HasTrait("SlowLearner") then
        Consumption = Consumption / 0.7
    end
    playerObj:getXp():AddXP(Perks.MineEndurance, math.ceil(-Consumption));
    local tmpXP = playerObj:getXp():getXP(Perks.MineEndurance)
    if playerObj:HasTrait("FastLearner") then
        tmpXP = tmpXP / 1.3
    end
    if playerObj:HasTrait("SlowLearner") then
        tmpXP = tmpXP / 0.7
    end
    playerObj:getXp():setXPToLevel(Perks.MineEndurance, 0)
    playerObj:getXp():AddXP(Perks.MineEndurance, math.ceil(tmpXP * 4));
end

--ФУНКЦИЯ "РЫТЬСЯ НА СВАЛКЕ"
ISSearchTrash = ISBaseTimedAction:derive("ISSearchTrash");

function ISSearchTrash:isValid()
    return self.character:getXp():getXP(Perks.MineEndurance) >= 15 and self.character:getStats():getEndurance() >= 0.1 and self.character:getStats():getFatigue() <= 0.8
end

function ISSearchTrash:update()
    self.character:faceThisObjectAlt(self.item)
    if not self.character:getEmitter():isPlaying(self.sound) then
        self.sound = self.character:playSound("Trash_Searching")
    end
end

function ISSearchTrash:start()
    self:setActionAnim("Loot")
    self.character:SetVariable("LootPosition", "Low")
    self.sound = self.character:playSound("Trash_Searching")
end

function ISSearchTrash:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
end

function ISSearchTrash:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    local r = ZombRand(100)
    local x = ZombRand(8)
    local bc = ZombRand(6)
    local dt = ZombRand(8)
    local nail = ZombRand(5)
    local screw = ZombRand(5)
    local plastic = ZombRand(3)
    local rope = ZombRand(10)
    local rubber = ZombRand(4)
    local silicon = ZombRand(4)
    local glass = ZombRand(3)
    local grncrd = ZombRand(30)
    local trshr = ZombRand(100)
    if r <= 50 then
        self.character:getInventory():AddItem("Base.ScrapMetal");
    end
    if x == 5 then
        self.character:getInventory():AddItems("Base.UnusableMetal", 3);
    elseif x == 4 then
        self.character:getInventory():AddItems("Base.UnusableMetal", 2);
    elseif x == 2 or x == 1 or x == 0 then
        self.character:getInventory():AddItem("Base.UnusableMetal");
    end
    if bc == 5 then
        self.character:getInventory():AddItems("NHM.BulletCasing", 2);
    elseif bc == 4 or bc == 3 then
        self.character:getInventory():AddItem("NHM.BulletCasing");
    end
    if dt == 0 then
        self.character:getInventory():AddItem("Base.DuctTape");
    elseif dt == 1 then
        self.character:getInventory():AddItem("Base.Glue");
    elseif dt == 2 then
        self.character:getInventory():AddItem("Base.Woodglue");
    elseif dt == 3 then
        self.character:getInventory():AddItem("Base.Scotchtape");
    end
    if nail == 2 or nail == 3 then
        self.character:getInventory():AddItem("Base.Nails");
    end
    if screw == 2 then
        self.character:getInventory():AddItems("Base.Screws", 2);
    end
    if plastic == 2 then
        self.character:getInventory():AddItem("NHM.Plastic");
    end
    if rope == 9 then
        self.character:getInventory():AddItem("Base.Rope");
    end
    if rubber == 3 then
        self.character:getInventory():AddItem("NHM.Rubber");
    end
    if silicon == 1 then
        self.character:getInventory():AddItem("NHM.SiliconOre");
    end
    if glass == 2 then
        self.character:getInventory():AddItem("NHM.Glass");
    end
    if grncrd == 9 then
        self.character:getInventory():AddItem("NHM.GreenCard");
    end
    if self.character:HasTrait('trasher') then
        if trshr <= 40 then
            if trshr >= 0 and trshr <= 4 then
                self.character:getInventory():AddItem("NHM.DyeBlack");
            elseif trshr >= 5 and trshr <= 9 then
                self.character:getInventory():AddItem("NHM.DyeOrange");
            elseif trshr >= 10 and trshr <= 14 then
                self.character:getInventory():AddItem("NHM.DyePink");
            elseif trshr >= 15 and trshr <= 19 then
                self.character:getInventory():AddItem("NHM.DyeCyan");
            elseif trshr >= 20 and trshr <= 24 then
                self.character:getInventory():AddItem("NHM.DyeYellow");
            elseif trshr >= 25 and trshr <= 29 then
                self.character:getInventory():AddItem("NHM.DyeRed");
            elseif trshr >= 30 and trshr <= 34 then
                self.character:getInventory():AddItem("NHM.DyeBrown");
            elseif trshr >= 35 and trshr <= 39 then
                self.character:getInventory():AddItem("NHM.DyeBlue");
            end
        end
    end

    ChangeEnergyLevelWithSync(self.character, 1)
    self.character:getStats():setEndurance(self.character:getStats():getEndurance() - 0.15);
end

function ISSearchTrash:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = time;
    return o;
end

--ВХОД В ЛАБУ
ISLabAction = ISBaseTimedAction:derive("ISLabAction");

function ISLabAction:isValid()
    return true
end

function ISLabAction:waitToStart()
    if self.direction == "N" then
        self.character:facePosition(self.character:getX(), self.character:getY() - 1)
    elseif self.direction == "W" then
        self.character:facePosition(self.character:getX() - 1, self.character:getY())
    end
    return self.character:shouldBeTurning()
end

function ISLabAction:update()
    if self.item then
        self.item:setJobDelta(self:getJobDelta());
    end
end

function ISLabAction:start()
    self:setActionAnim("TakeGasFromPump")
    if self.item then
        self.item:setJobType(getText("ContextMenu_UseKeycard"));
        self.item:setJobDelta(0.0);
    end
    self.sound = self.character:playSound("CardAccess")
end

function ISLabAction:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    if self.item then
        self.item:setJobDelta(0.0);
    end
    ISBaseTimedAction.stop(self);
end

function ISLabAction:perform()
    ISBaseTimedAction.perform(self);
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    if self.item then
        self.item:setJobDelta(0.0);

        if self.item:getDrainableUsesInt() ~= 50 then
            self.item:setUsedDelta(self.item:getUsedDelta() - 0.5);
        else
            self.character:removeFromHands(self.item)
            self.character:getInventory():removeItemWithIDRecurse(self.item:getID())
        end
    end
    ebanie_cordinati.x = self.x
    ebanie_cordinati.y = self.y
    ebanie_cordinati.z = self.z
    Events.OnTick.Add(pornuxa)
end

function ISLabAction:new(character, tile, item, x, y, z, direction)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.tile = tile;
    o.item = item
    o.maxTime = 45
    o.x = x
    o.y = y
    o.z = z
    o.direction = direction
    return o;
end

--ФУНКЦИЯ "НАПОЛНИТЬ ГАЗОВУЮ ГОРЕЛКУ / БАЛЛОН С ПРОПАНОМ"
ISExtractGas = ISBaseTimedAction:derive("ISExtractGas");

function ISExtractGas:isValid()
    return self.character:getXp():getXP(Perks.MineEndurance) >= 15 * self.level and self.character:getStats():getEndurance() >= 0.1 and self.character:getStats():getFatigue() <= 0.8 and self.character:getPrimaryHandItem():getDrainableUsesInt() ~= 100
end

function ISExtractGas:waitToStart()
    self.character:faceLocation(self.tile:getSquare():getX(), self.tile:getSquare():getY())
    return self.character:shouldBeTurning()
end

function ISExtractGas:update()
    self.item:setJobDelta(self:getJobDelta());
    self.character:faceThisObjectAlt(self.tile)
    if not self.character:getEmitter():isPlaying(self.sound) then
        self.sound = self.character:playSound("Propane_Filling")
    end
end

function ISExtractGas:start()
    self.item:setJobType(getText("ContextMenu_TakeGasFromGasStation"));
    self.item:setJobDelta(0.0);

    self:setOverrideHandModels(self.item:getStaticModel(), nil)
    self:setActionAnim("TakeGasFromVehicle")
    self.sound = self.character:playSound("Propane_Filling")
end

function ISExtractGas:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    self.item:setJobDelta(0.0);
    ISBaseTimedAction.stop(self);
end

function ISExtractGas:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.item:setJobDelta(0.0);

    self.item:setDelta(1)

    ChangeEnergyLevelWithSync(self.character, self.level)
    self.character:getStats():setEndurance(self.character:getStats():getEndurance() - 0.2);
end

function ISExtractGas:new(character, tile, item, oldDelta, time, level)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.tile = tile;
    o.item = item
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = time * math.ceil((1 - oldDelta) * 10) / 10;
    o.level = level
    return o;
end


--ФУНКЦИЯ ДЛЯ ШАХТЫ
ISMineGeneral = ISBaseTimedAction:derive("ISMineGeneral");

function ISMineGeneral:isValid()
    return self.character:getXp():getXP(Perks.MineEndurance) >= 15 * self.level and self.character:getStats():getEndurance() >= 0.1 and self.character:getStats():getFatigue() <= 0.8
end

function ISMineGeneral:waitToStart()
    self.character:faceThisObjectAlt(self.item)
    return self.character:shouldBeTurning()
end

function ISMineGeneral:update()
    self.PickAxe:setJobDelta(self:getJobDelta());
    self.character:faceThisObjectAlt(self.item)
    if not self.character:getEmitter():isPlaying(self.sound) then
        self.sound = self.character:playSound("Mining_Pickaxe")
    end
end

function ISMineGeneral:start()
    self.PickAxe:setJobType(getText("ContextMenu_Mining"));
    self.PickAxe:setJobDelta(0.0);

    self:setActionAnim("Mining")
    self:setOverrideHandModels(self.PickAxe, nil)
    self.sound = self.character:playSound("Mining_Pickaxe")
end

function ISMineGeneral:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    self.PickAxe:setJobDelta(0.0);
    ISBaseTimedAction.stop(self);
end

function ISMineGeneral:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.PickAxe:setJobDelta(0.0);

    --copper
    if self.OreSpriteName == "NHCopper_0" then
        local minecop = ZombRand(5)
        self.character:getXp():AddXP(Perks.MetalWelding, 5);
        if minecop == 3 or (minecop == 2 and self.character:HasTrait('miner')) or (minecop == 1 and self.character:HasTrait('miner')) then
            self.character:getInventory():AddItems("NHM.CopperOre", 2);
        else
            self.character:getInventory():AddItem("NHM.CopperOre");
        end
    end

    --tin
    if self.OreSpriteName == "NHTin_0" then
        local minetin = ZombRand(5)
        self.character:getXp():AddXP(Perks.MetalWelding, 5);
        if minetin == 3 or (minetin == 2 and self.character:HasTrait('miner')) or (minetin == 1 and self.character:HasTrait('miner')) then
            self.character:getInventory():AddItems("NHM.TinOre", 2);
        else
            self.character:getInventory():AddItem("NHM.TinOre");
        end
    end

    --iron
    if self.OreSpriteName == "NHIron_0" then
        local mineiron = ZombRand(5)
        self.character:getXp():AddXP(Perks.MetalWelding, 10);
        if mineiron == 4 or (mineiron == 2 and self.character:HasTrait('miner')) or (mineiron == 1 and self.character:HasTrait('miner')) then
            self.character:getInventory():AddItems("NHM.IronOre", 2);
        else
            self.character:getInventory():AddItem("NHM.IronOre");
        end
        self.character:getInventory():AddItems("Base.Stone", 1);
    end

    --lead
    if self.OreSpriteName == "NHLead_0" then
        local minelead = ZombRand(5)
        self.character:getXp():AddXP(Perks.MetalWelding, 10);
        if minelead == 4 or (minelead == 2 and self.character:HasTrait('miner')) or (minelead == 1 and self.character:HasTrait('miner')) then
            self.character:getInventory():AddItems("NHM.GalenaOre", 2);
        else
            self.character:getInventory():AddItem("NHM.GalenaOre");
        end
        self.character:getInventory():AddItems("Base.Stone", 1);
    end

    --nickel
    if self.OreSpriteName == "NHNickel_0" then
        local minenkl = ZombRand(5)
        self.character:getXp():AddXP(Perks.MetalWelding, 15);
        if minenkl == 4 or (minenkl == 2 and self.character:HasTrait('miner')) or (minenkl == 1 and self.character:HasTrait('miner')) then
            self.character:getInventory():AddItems("NHM.NickelOre", 2);
        else
            self.character:getInventory():AddItem("NHM.NickelOre");
        end
        self.character:getInventory():AddItems("Base.Stone", 2);
    end

    --chromium
    if self.OreSpriteName == "NHChromium_0" then
        local minechr = ZombRand(5)
        self.character:getXp():AddXP(Perks.MetalWelding, 15);
        if minechr == 4 or (minechr == 2 and self.character:HasTrait('miner')) or (minechr == 1 and self.character:HasTrait('miner')) then
            self.character:getInventory():AddItems("NHM.ChromiumOre", 2);
        else
            self.character:getInventory():AddItem("NHM.ChromiumOre");
        end
        self.character:getInventory():AddItems("Base.Stone", 2);
    end

    --sulfur
    if self.OreSpriteName == "NHSulfur_0" then
        local mineslfr = ZombRand(5)
        self.character:getXp():AddXP(Perks.MetalWelding, 6);
        if mineslfr == 4 or (mineslfr == 2 and self.character:HasTrait('miner')) or (mineslfr == 1 and self.character:HasTrait('miner')) then
            self.character:getInventory():AddItems("NHM.SulfurOre", 3);
        else
            self.character:getInventory():AddItems("NHM.SulfurOre", 2);
        end
    end

    --limestone
    if self.OreSpriteName == "NHLimestone_0" then
        local minelmst = ZombRand(5)
        self.character:getXp():AddXP(Perks.MetalWelding, 3);
        if minelmst == 4 or (minelmst == 2 and self.character:HasTrait('miner')) or (minelmst == 1 and self.character:HasTrait('miner')) then
            self.character:getInventory():AddItems("NHM.Limestone", 2);
        else
            self.character:getInventory():AddItem("NHM.Limestone");
        end
    end

    --coal
    if self.OreSpriteName == "NHCoal_0" then
        local minecl = ZombRand(5)
        self.character:getXp():AddXP(Perks.MetalWelding, 3);
        if minecl == 4 or (minecl == 2 and self.character:HasTrait('miner')) or (minecl == 1 and self.character:HasTrait('miner')) then
            self.character:getInventory():AddItems("NHM.Coal", 5);
        else
            self.character:getInventory():AddItems("NHM.Coal", 3);
        end
    end

    --stone
    if self.OreSpriteName == "NHStone_1_0" or self.OreSpriteName == "NHStone_2_0" then
        local minestn = ZombRand(5)
        self.character:getXp():AddXP(Perks.MetalWelding, 6);
        if minestn == 4 or (minestn == 2 and self.character:HasTrait('miner')) or (minestn == 1 and self.character:HasTrait('miner')) then
            self.character:getInventory():AddItems("Base.Stone", 5);
        else
            self.character:getInventory():AddItems("Base.Stone", 3);
        end
    end

    --salt
    if self.OreSpriteName == "NHSalt_1_0" or self.OreSpriteName == "NHSalt_2_0" then
        local mineslt = ZombRand(5)
        self.character:getXp():AddXP(Perks.MetalWelding, 3);
        if mineslt == 4 or (mineslt == 2 and self.character:HasTrait('miner')) or (mineslt == 1 and self.character:HasTrait('miner')) then
            self.character:getInventory():AddItems("NHM.SaltPinch", 4);
        else
            self.character:getInventory():AddItems("NHM.SaltPinch", 2);
        end
    end

    ChangeEnergyLevelWithSync(self.character, self.level)
    self.character:getStats():setEndurance(self.character:getStats():getEndurance() - self.endurance);

end

function ISMineGeneral:new(character, item, PickAxe, time, OreSpriteName, level, endurance)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.PickAxe = PickAxe;
    o.OreSpriteName = OreSpriteName
    o.level = level
    o.endurance = endurance
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = time;
    if character:HasTrait("miner") then
        o.maxTime = time / 1.5
    end
    if character:HasTrait("Claustrophobic") then
        o.maxTime = time * 1.3
    end
    if character:isTimedActionInstant() then
        o.maxTime = 1;
    end
    return o;
end


--ФУНКЦИЯ ДОБЫТЬ АКТИВНОЕ ВЕЩЕСТВО
ISActiveComp = ISBaseTimedAction:derive("ISActiveComp");

function ISActiveComp:isValid()
    return self.character:getXp():getXP(Perks.MineEndurance) >= 60 and self.character:getStats():getEndurance() >= 0.1 and self.character:getStats():getFatigue() <= 0.8
end

function ISActiveComp:update()
    self.character:faceThisObjectAlt(self.item)
    if not self.character:getEmitter():isPlaying(self.sound) then
        self.sound = self.character:playSound("ActiveComponent")
    end
end

function ISActiveComp:start()
    self:setActionAnim("Craft")
    self.sound = self.character:playSound("ActiveComponent")
end

function ISActiveComp:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
end

function ISActiveComp:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:removeFromHands(self.Flask)
    self.character:getInventory():Remove("ChemicalFlask");
    self.character:getInventory():AddItem("NHM.FlaskActiveComp");
    ChangeEnergyLevelWithSync(self.character, 4)
end

function ISActiveComp:new(character, item, time, Flask)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = time;
    o.Flask = Flask
    return o;
end

--ФУНКЦИЯ НАПОЛНИТЬ КАНИСТРУ БЕНЗИНОМ
ISExtractPetrol = ISBaseTimedAction:derive("ISExtractPetrol");

function ISExtractPetrol:isValid()
    return self.character:getXp():getXP(Perks.MineEndurance) >= 15 * self.level and self.character:getStats():getEndurance() >= 0.1 and self.character:getStats():getFatigue() <= 0.8
end

function ISExtractPetrol:waitToStart()
    self.character:faceLocation(self.tile:getSquare():getX(), self.tile:getSquare():getY())
    return self.character:shouldBeTurning()
end

function ISExtractPetrol:update()
    self.item:setJobDelta(self:getJobDelta());
    self.character:faceThisObjectAlt(self.tile)
    if not self.character:getEmitter():isPlaying(self.sound) then
        self.sound = self.character:playSound("CanisterAddFuelFromGasPump")
    end
end

function ISExtractPetrol:start()
    self.item:setJobType(getText("ContextMenu_TakeGasFromPump"));
    self.item:setJobDelta(0.0);

    self:setOverrideHandModels(nil, self.item:getStaticModel())
    self:setActionAnim("TakeGasFromPump")
    self.sound = self.character:playSound("CanisterAddFuelFromGasPump")
end

function ISExtractPetrol:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    self.item:setJobDelta(0.0);
    ISBaseTimedAction.stop(self);
end

function ISExtractPetrol:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.item:setJobDelta(0.0);

    if self.item:getType() == "EmptyPetrolCan" then
        local emptyCan = self.item
        self.item = self.character:getInventory():AddItem("Base.PetrolCan")
        if self.character:getPrimaryHandItem() == emptyCan then
            self.character:setPrimaryHandItem(self.item)
        end
        if self.character:getSecondaryHandItem() == emptyCan then
            self.character:setSecondaryHandItem(self.item)
        end
        self.character:getInventory():Remove(emptyCan)
    else
        self.item:setDelta(1)
    end

    ChangeEnergyLevelWithSync(self.character, self.level)
    self.character:getStats():setEndurance(self.character:getStats():getEndurance() - 0.5);
end

function ISExtractPetrol:new(character, item, tile, level, time, oldDelta)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.tile = tile
    o.level = level
    o.stopOnWalk = true;
    o.stopOnRun = true;
    if oldDelta then
        o.maxTime = time * math.ceil((1 - oldDelta) * 10) / 10;
    else
        o.maxTime = time
    end
    return o;
end

--ФУНКЦИЯ ДОБЫТЬ ДЕТАЛИ ОРУЖИЯ
ISExtractWeaponpart = ISBaseTimedAction:derive("ISExtractWeaponpart");

function ISExtractWeaponpart:isValid()
    return true;
end

function ISExtractWeaponpart:update()
end

function ISExtractWeaponpart:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
end

function ISExtractWeaponpart:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
end

function ISExtractWeaponpart:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    local wp = ZombRand(33)
    if wp == 0 or wp == 1 then
        self.character:getInventory():AddItem("NHM.BarrelRifle");
    elseif wp == 2 or wp == 3 then
        self.character:getInventory():AddItem("NHM.BarrelPistol");
    elseif wp == 4 or wp == 5 then
        self.character:getInventory():AddItem("NHM.Riflebody");
    elseif wp == 6 or wp == 7 then
        self.character:getInventory():AddItem("NHM.Woodenparts");
    elseif wp == 8 or wp == 9 then
        self.character:getInventory():AddItem("NHM.Triggergroup");
    elseif wp == 10 or wp == 11 then
        self.character:getInventory():AddItem("NHM.Boltgroup");
    elseif wp == 12 or wp == 13 then
        self.character:getInventory():AddItem("NHM.BoltgroupSA");
    elseif wp == 14 or wp == 15 then
        self.character:getInventory():AddItem("NHM.Gastube");
    elseif wp == 16 or wp == 17 then
        self.character:getInventory():AddItem("NHM.Spring");
    elseif wp == 18 or wp == 19 then
        self.character:getInventory():AddItem("NHM.Revolverdrum");
    elseif wp == 20 or wp == 21 then
        self.character:getInventory():AddItem("NHM.Pistolbody");
    end
end

function ISExtractWeaponpart:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end


--КВЕСТ 1 УБОРЩИЦА МЫТЬ ПОЛЫ
ISElevatorDirt = ISBaseTimedAction:derive("ISElevatorDirt");

function ISElevatorDirt:isValid()
    return true;
end

function ISElevatorDirt:update()
end

function ISElevatorDirt:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHBroom");
    self.character:getInventory():Remove("QuestHBucket");
end

function ISElevatorDirt:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHBroom");
    self.character:getInventory():AddItem("NHM.QuestHBucket");
end

function ISElevatorDirt:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHBroomDirty");
    self.character:getInventory():AddItem("NHM.QuestHBucketDirty");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/ElevatorDirt.png>" .. "<LINE><LINE>" .. getText("Tooltip_CleanerQuest1_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISElevatorDirt:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end


--КВЕСТ 2 УБОРЩИЦА СТИРАТЬ ОДЕЖДУ
ISLaundry = ISBaseTimedAction:derive("ISLaundry");

function ISLaundry:isValid()
    return true;
end

function ISLaundry:update()
end

function ISLaundry:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHDirtyCloth");
end

function ISLaundry:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHDirtyCloth");
end

function ISLaundry:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHCleanCloth");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/Laundry.png>" .. "<LINE><LINE>" .. getText("Tooltip_CleanerQuest2_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISLaundry:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- ДЕМИУРГ ЗАДАНИЕ 1 АРХИВ
ISArchive = ISBaseTimedAction:derive("ISArchive");

function ISArchive:isValid()
    return true;
end

function ISArchive:update()
end

function ISArchive:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHTip");
end

function ISArchive:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHTip");
end

function ISArchive:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHArchive");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/Archive.png>" .. "<LINE><LINE>" .. getText("Tooltip_DemiurgeQuest1_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISArchive:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- ДЕМИУРГ ЗАДАНИЕ 2 ПИЛА 
ISMorgue = ISBaseTimedAction:derive("ISMorgue");

function ISMorgue:isValid()
    return true;
end

function ISMorgue:update()
end

function ISMorgue:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHTweezers");
end

function ISMorgue:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHTweezers");
end

function ISMorgue:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHDeadSaw");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/Morgue.png>" .. "<LINE><LINE>" .. getText("Tooltip_DemiurgeQuest2_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISMorgue:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- СЕСТРА ЗАДАНИЕ 1 ДИСКЕТА 
ISComputer = ISBaseTimedAction:derive("ISComputer");

function ISComputer:isValid()
    return true;
end

function ISComputer:update()
end

function ISComputer:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHDisk");
end

function ISComputer:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHDisk");
end

function ISComputer:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHFullDisk");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/Computer.png>" .. "<LINE><LINE>" .. getText("Tooltip_NurseQuest1_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISComputer:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- СЕСТРА ЗАДАНИЕ 2 КОЛБА
ISVioletLiquid = ISBaseTimedAction:derive("ISVioletLiquid");

function ISVioletLiquid:isValid()
    return true;
end

function ISVioletLiquid:update()
end

function ISVioletLiquid:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHEmptyFlask");
end

function ISVioletLiquid:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHEmptyFlask");
end

function ISVioletLiquid:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHFullFlask");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/VioletLiquid.png>" .. "<LINE><LINE>" .. getText("Tooltip_NurseQuest2_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISVioletLiquid:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- АССИСТЕНТ ЗАДАНИЕ 1 ПЕТРИ 
ISMicroscope = ISBaseTimedAction:derive("ISMicroscope");

function ISMicroscope:isValid()
    return true;
end

function ISMicroscope:update()
end

function ISMicroscope:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHPetri");
end

function ISMicroscope:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHPetri");
end

function ISMicroscope:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHFullPetri");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/Microscope.png>" .. "<LINE><LINE>" .. getText("Tooltip_AssistantQuest1_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISMicroscope:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- АССИСТЕНТ ЗАДАНИЕ 2 КАРТА ЗВЕЗДНОГО НЕБА
ISStars = ISBaseTimedAction:derive("ISStars");

function ISStars:isValid()
    return true;
end

function ISStars:update()
end

function ISStars:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHEmptyStars");
end

function ISStars:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHEmptyStars");
end

function ISStars:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHStars");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/Stars.png>" .. "<LINE><LINE>" .. getText("Tooltip_AssistantQuest2_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISStars:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end


-- КУХАРКА ЗАДАНИЕ 1 ТАРЕЛКИ 
ISSink = ISBaseTimedAction:derive("ISSink");

function ISSink:isValid()
    return true;
end

function ISSink:update()
end

function ISSink:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHDirtyDish");
end

function ISSink:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHDirtyDish");
end

function ISSink:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHCleanDish");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/Sink.png>" .. "<LINE><LINE>" .. getText("Tooltip_CookQuest1_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISSink:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- КУХАРКА ЗАДАНИЕ 2 ПЕВКО))
ISBeer = ISBaseTimedAction:derive("ISBeer");

function ISBeer:isValid()
    return true;
end

function ISBeer:update()
end

function ISBeer:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHEmptyBeer");
end

function ISBeer:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHEmptyBeer");
end

function ISBeer:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHBeer");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/Beer.png>" .. "<LINE><LINE>" .. getText("Tooltip_CookQuest2_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISBeer:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- ЗАВЕДУЮЩИЙ ЗАДАНИЕ 1 ОГНЕТУШИТЕЛЬ
ISFirecup = ISBaseTimedAction:derive("ISFirecup");

function ISFirecup:isValid()
    return true;
end

function ISFirecup:update()
end

function ISFirecup:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHKey");
end

function ISFirecup:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHKey");
end

function ISFirecup:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHFireEx");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/Firecup.png>" .. "<LINE><LINE>" .. getText("Tooltip_HeadQuest1_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISFirecup:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- ЗАВЕДУЮЩИЙ ЗАДАНИЕ 2 ПАКЕТ КРОВИ
ISBlood = ISBaseTimedAction:derive("ISBlood");

function ISBlood:isValid()
    return true;
end

function ISBlood:update()
end

function ISBlood:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHBloodBag");
end

function ISBlood:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHBloodBag");
end

function ISBlood:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHFullBloodBag");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/Blood.png>" .. "<LINE><LINE>" .. getText("Tooltip_HeadQuest2_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISBlood:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- ФАРМАЦЕВТ ЗАДАНИЕ 1 ТАБЛЕТКИ
ISTablets = ISBaseTimedAction:derive("ISTablets");

function ISTablets:isValid()
    return true;
end

function ISTablets:update()
end

function ISTablets:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestHMortPest");
end

function ISTablets:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestHMortPest");
end

function ISTablets:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestHTablets");
    QuestWindow:setText("<IMAGECENTRE:media/textures/Quests/Tablets.png>" .. "<LINE><LINE>" .. getText("Tooltip_PharmacistQuest1_Perform") .. "\n")
    QuestWindow:setVisible(true);
end

function ISTablets:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- КАИН ЗАДАНИЕ НА АВТОМАТ 1
ISComputer2 = ISBaseTimedAction:derive("ISComputer2");

function ISComputer2:isValid()
    return true;
end

function ISComputer2:update()
end

function ISComputer2:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestAFlash");
end

function ISComputer2:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestAFlash");
end

function ISComputer2:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestAEmptyFlash");
end

function ISComputer2:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

-- КАИН ЗАДАНИЕ НА АВТОМАТ 2
ISComputer3 = ISBaseTimedAction:derive("ISComputer3");

function ISComputer3:isValid()
    return true;
end

function ISComputer3:update()
end

function ISComputer3:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
    self.character:getInventory():Remove("QuestADisk");
end

function ISComputer3:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
    self.character:getInventory():AddItem("NHM.QuestADisk");
end

function ISComputer3:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.QuestAFullDisk");
end

function ISComputer3:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end


--ФУНКЦИЯ КАЧАТЬ БЕГ
ISTrainRun = ISBaseTimedAction:derive("ISTrainRun");

function ISTrainRun:isValid()
    return true;
end

function ISTrainRun:update()
end

function ISTrainRun:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
end

function ISTrainRun:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
end

function ISTrainRun:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    local g = getPlayer():getPerkLevel(Perks.Sprinting);
    getPlayer():getXp():AddXP(Perks.Sprinting, 7.5 * (1 + g));
end

function ISTrainRun:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = time;
    return o;
end

--ФУНКЦИЯ КАЧАТЬ ЛЕГКИЙ ШАГ
ISTrainLightfoot = ISBaseTimedAction:derive("ISTrainLightfoot");

function ISTrainLightfoot:isValid()
    return true;
end

function ISTrainLightfoot:update()
end

function ISTrainLightfoot:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
end

function ISTrainLightfoot:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
end

function ISTrainLightfoot:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    local g = getPlayer():getPerkLevel(Perks.Lightfoot);
    getPlayer():getXp():AddXP(Perks.Lightfoot, 7.5 * (1 + g));
end

function ISTrainLightfoot:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = time;
    return o;
end

--ФУНКЦИЯ КАЧАТЬ ПРОВОРНОСТЬ
ISTrainNimble = ISBaseTimedAction:derive("ISTrainNimble");

function ISTrainNimble:isValid()
    return true;
end

function ISTrainNimble:update()
end

function ISTrainNimble:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
end

function ISTrainNimble:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
end

function ISTrainNimble:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    local g = getPlayer():getPerkLevel(Perks.Nimble);
    getPlayer():getXp():AddXP(Perks.Nimble, 7.5 * (1 + g));
end

function ISTrainNimble:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = time;
    return o;
end

--ФУНКЦИЯ КАЧАТЬ СКРЫТНОСТЬ
ISTrainSneak = ISBaseTimedAction:derive("ISTrainSneak");

function ISTrainSneak:isValid()
    return true;
end

function ISTrainSneak:update()
end

function ISTrainSneak:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
end

function ISTrainSneak:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
end

function ISTrainSneak:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    local g = getPlayer():getPerkLevel(Perks.Sneak);
    getPlayer():getXp():AddXP(Perks.Sneak, 7.5 * (1 + g));
end

function ISTrainSneak:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = time;
    return o;
end

--ФУНКЦИЯ ВЗЛОМАТЬ БАНКОМАТ
ISRobBank = ISBaseTimedAction:derive("ISRobBank");

function ISRobBank:isValid()
    return true;
end

function ISRobBank:update()
end

function ISRobBank:start()
    self.sound = self.character:playSound("TrashSearch");
    local radius = 20
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), radius, radius)
end

function ISRobBank:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
end

function ISRobBank:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    local hackmoney = ZombRand(5)
    self.character:getInventory():Remove("CreditCard");
    if hackmoney == 4 then
        self.character:getInventory():AddItems("Base.Money", 4);
    elseif hackmoney == 3 or hackmoney == 2 then
        self.character:getInventory():AddItems("Base.Money", 3);
    else
        self.character:getInventory():AddItems("Base.Money", 2);
    end
end

function ISRobBank:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = 950;
    return o;
end

--ПОИСК МИКРОСХЕМ
ISSearchChip = ISBaseTimedAction:derive("ISSearchChip");

function ISSearchChip:isValid()
    return self.character:getXp():getXP(Perks.MineEndurance) >= 45 and self.character:getStats():getEndurance() >= 0.1 and self.character:getStats():getFatigue() <= 0.8
end

function ISSearchChip:update()
    self.character:faceThisObjectAlt(self.item)
    if not self.character:getEmitter():isPlaying(self.sound) then
        self.sound = self.character:playSound("Trash_Searching")
    end
end

function ISSearchChip:start()
    self:setActionAnim("Loot")
    self.character:SetVariable("LootPosition", "Low")
    self.sound = self.character:playSound("Trash_Searching")
end

function ISSearchChip:stop()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.stop(self);
end

function ISSearchChip:perform()
    if self.sound then
        self.character:getEmitter():stopSound(self.sound)
        self.sound = nil
    end
    ISBaseTimedAction.perform(self);
    self.character:getInventory():AddItem("NHM.Microchip");
    ChangeEnergyLevelWithSync(self.character, 3)
    self.character:getStats():setEndurance(self.character:getStats():getEndurance() - 0.15);
end

function ISSearchChip:new(character, item, time)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character;
    o.item = item;
    o.stopOnWalk = true;
    o.stopOnRun = true;
    o.maxTime = time;
    return o;
end

function pornuxa()
    print('goin ' .. ebanie_cordinati.x .. " " .. ebanie_cordinati.y .. " " .. ebanie_cordinati.z)
    getPlayer():setX(ebanie_cordinati.x)
    getPlayer():setY(ebanie_cordinati.y)
    getPlayer():setZ(ebanie_cordinati.z)
    getPlayer():setLx(ebanie_cordinati.x)
    getPlayer():setLy(ebanie_cordinati.y)
    getPlayer():setLz(ebanie_cordinati.z)
    Events.OnTick.Remove(pornuxa)
end