#is_flag ghost_intro,true
    #jump loop

*intro
#m Призрак|NPCPortraits/GhostP.png|Незнакомец в маске смотрит в вашу сторону.

#choice
    intro_hello|Познакомиться
    intro_leave|Уйти

*intro_leave
#m Призрак|NPCPortraits/GhostP.png|...
#exit

*intro_hello
#m "Прошу прощения... Я тут освоиться пытаюсь... Вы можете мне что-нибудь рассказать?"
#m Призрак|NPCPortraits/GhostP.png|Незнакомец в маске медленно поднимает правую руку и показывает пальцем на нашивку на груди. Вы вглядываетесь и читаете "П-Р-И-З-Р-А-К".
#m "Ага! Призрак - это ваше имя?" 
#m Призрак|NPCPortraits/GhostP.png|Незнакомец в маске кивает и продолжает смотреть на вас.
#m "Ясно... Не очень-то вы и общительны! Вы вообще умеете разговаривать?"
#m Призрак|NPCPortraits/GhostP.png|Незнакомец в маске поворачивается так, словно под маской смотрит вам прямо в глаза.
#m "Так, хорошо... Больше не спрашиваю... Может, и у вас есть для меня работа?"
#fade_out black
#set_flag ghost_intro,true
#fade_in
#reveal Ghost
#exit

*loop
#m Призрак|NPCPortraits/GhostP.png|Призрак громко вздыхает. Вы слышите в этом вздохе раздраженность.

#choice
    questGHOST|Взять задание?
    quest_exit|Уйти

*questGHOST
#has_item Base.Money,5
    #is_event GhostQuest|false
        #remove_item Base.Money,5
        #m Призрак|NPCPortraits/GhostP.png|Призрак протягивает вам записку.
        #m Этот предмет приведет тебя к одному из моих схронов в округе Нокс. Используй с умом. Устрйоство покажет векторное расстояние до точки и самоуничтожится в радиусе нескольких метров возле схрона.
        #m "Ага, понятно! Спасибо."
        #m Призрак|NPCPortraits/GhostP.png|Призрак медленно отводит взгляд.
        #set_event GhostQuest|true|12
        #roll take_1,5|take_2,10|take_3,15|take_4,20|take_5,25|take_6,30|take_7,35|take_8,40|take_9,45|take_10,50|take_11,55|take_12,60|take_13,65|take_14,70|take_15,75|take_16,80|take_17,85|take_18,90|take_19,95|take_20,100|
        #exit
    #m Призрак|NPCPortraits/GhostP.png|Призрак качает головой, показывая пальцем на часы. 
    #exit
#m Призрак|NPCPortraits/GhostP.png|Призрак качает головой, показывая жестами, что за это задание нужно заплатить 5 долларов.
#exit

*quest_exit
#exit

*take_1
#reward NHM.GhostQuest1,1,rare
#exit

*take_2
#reward NHM.GhostQuest2,1,rare
#exit

*take_3
#reward NHM.GhostQuest3,1,rare
#exit

*take_4
#reward NHM.GhostQuest4,1,rare
#exit

*take_5
#reward NHM.GhostQuest5,1,rare
#exit

*take_6
#reward NHM.GhostQuest6,1,rare
#exit

*take_7
#reward NHM.GhostQuest7,1,rare
#exit

*take_8
#reward NHM.GhostQuest8,1,rare
#exit

*take_9
#reward NHM.GhostQuest9,1,rare
#exit

*take_10
#reward NHM.GhostQuest10,1,rare
#exit

*take_11
#reward NHM.GhostQuest11,1,rare
#exit

*take_12
#reward NHM.GhostQuest12,1,rare
#exit

*take_13
#reward NHM.GhostQuest13,1,rare
#exit

*take_14
#reward NHM.GhostQuest14,1,rare
#exit

*take_15
#reward NHM.GhostQuest15,1,rare
#exit

*take_16
#reward NHM.GhostQuest16,1,rare
#exit

*take_17
#reward NHM.GhostQuest17,1,rare
#exit

*take_18
#reward NHM.GhostQuest18,1,rare
#exit

*take_19
#reward NHM.GhostQuest19,1,rare
#exit

*take_20
#reward NHM.GhostQuest20,1,rare
#exit