#is_flag aspirin_intro,true
    #jump loop

*intro
#m Аспирин|NPCPortraits/AspirinP.png|"Твою мать, да сколько же вас здесь... Оазис мёдом что ли намазан?"
#m Аспирин|NPCPortraits/AspirinP.png|"Местные зовут меня Аспирин. Но ты можешь даже не запоминать, все равно долго не протянешь."

#choice
    intro_hello|Познакомиться
    intro_leave|Уйти

*intro_leave
#m Аспирин|NPCPortraits/AspirinP.png|"Слава богу, не придется с тобой нянчиться."
#exit

*intro_hello
#m "Эээ... здравствуй, Аспирин. Приятно познакомиться. Чем занимаешься?"
#m Аспирин|NPCPortraits/AspirinP.png|"Не твое собачье дело. Либо выполняешь задания, либо катись отсюда. В процессе расскажу пару лайфхаков, если настроение будет хорошее."
#fade_out black
#set_flag aspirin_intro,true
#fade_in
#reveal Aspirin
#exit

*loop
#m Аспирин|NPCPortraits/AspirinP.png|"Удивлена, что ты еще дышишь. ${female,Надумала,Надумал} приносить пользу или продолжишь бесцельно бродить?"

#is_quest aspirin_q1|locked
    #set_flag aspirin_q1_flag,false
#is_quest aspirin_q1|completed
    #set_flag aspirin_q1_flag,false
#is_quest aspirin_q1|failed
    #set_flag aspirin_q1_flag,false

#is_quest aspirin_q2|locked
    #set_flag aspirin_q2_flag,false
#is_quest aspirin_q2|completed
    #set_flag aspirin_q2_flag,false
#is_quest aspirin_q2|failed
    #set_flag aspirin_q2_flag,false

#is_quest aspirin_q3|locked
    #set_flag aspirin_q3_flag,false
#is_quest aspirin_q3|completed
    #set_flag aspirin_q3_flag,false
#is_quest aspirin_q3|failed
    #set_flag aspirin_q3_flag,false

#is_quest aspirin_q4|locked
    #set_flag aspirin_q4_flag,false
#is_quest aspirin_q4|completed
    #set_flag aspirin_q4_flag,false
#is_quest aspirin_q4|failed
    #set_flag aspirin_q4_flag,false

*backtolist
#choice
    questsASPIRIN|Обучающие задания
    questsASPIRIN2|Регулярные задания
    quest_exit|Уйти
    questsASPIRINabandon|Сбросить обучающие задания

*questsASPIRIN
#choice
    questASPIRIN1_1|Задание "Угольные таблетки"
    questASPIRIN1_2|Задание "Образцы зараженного"
    backtolist|Вернуться к общему списку
    questASPIRIN1_1_complete|Сдать Задание|aspirin_q1_flag
    questASPIRIN1_2_complete|Сдать Задание|aspirin_q2_flag

*questASPIRIN1_1
#is_quest aspirin_q2|unlocked,uncompleted,unfailed
    #m Аспирин|NPCPortraits/AspirinP.png|"Куда ${female,разогналась,разогнался}? По одному заданию за раз!"
    #exit
#is_quest aspirin_q1|locked
    #m Аспирин|NPCPortraits/AspirinP.png|"Ладно, я обещала Киоши, что буду учить новеньких. Только и ты мне обещай, что не скопытишься в течение часа... Итак, Первая помощь! Этот навык важнее всех других вместе взятых! Почему? Ну, зачем тебе знать как собирается оружие, если ты умрешь от обычного гриппа или от заражения крови? Вот и я о том же..."
    #m Аспирин|NPCPortraits/AspirinP.png|"Запоминай. Первое, что нужно уяснить - это наличие Химзаражения (или Биозаражения или просто Заражения). Некоторые называют его Радиацией, но это глупости. Была бы это радиация, мы бы... неважно. Заражение покрывает весь Луисвилл и шахты Хоуптауна. Возможно, где-то еще оно есть..."
    #m Аспирин|NPCPortraits/AspirinP.png|"Как от него спастись? Не ходить туда, очевидно. Но если очень нужно - обязательно с противогазом и фильтрами. Противогаз у нас один, а вот фильтры бывают разные, в зависимости от степени Заражения. В Луисвилле достаточно самых примитивных фильтров, а в Железной шахте нужны фильтры получше. Ну, ${female,сама,сам} разберешься."
    #m Аспирин|NPCPortraits/AspirinP.png|"Если же ты каким-то нелепым образом ${female,оказалась,оказался} в зоне Заражения без защиты и ${female,надышалась,надышался} местного воздуха - это еще не конец. Тебе нужны Угольные таблетки. Советую на всякий случай всегда иметь с собой парочку таких. Создаются довольно легко - из добра, добытого Собирательством. Сбрасывают болезнь. Лучше пить сразу несколько."
    #m Аспирин|NPCPortraits/AspirinP.png|"Собственно, с них мы и начнем. Принеси мне парочку угольных таблеток. Ищи уголь в лесах или съезди на гору Блэк и добудь его из местных залежей. Из 6 единиц угля получаются 2 угольные таблетки. Вот их мне и принесешь. Всё, топай, от важных дел отвлекаешь."
    #quest_unlock aspirin_q1
    #task_unlock aspirin_q1|t1 
    #set_flag aspirin_q1_flag,true
    #exit
#is_quest aspirin_q1|uncompleted,unfailed
    #m Аспирин|NPCPortraits/AspirinP.png|"Ты уже ${female,взяла,взял} это задание. Издеваешься?"
    #exit
#m Аспирин|NPCPortraits/AspirinP.png|"Хватит с тебя. Приходи после 9 утра."
#exit

*questASPIRIN1_1_complete
#deliver aspirin_q1|t1
    #m Аспирин|NPCPortraits/AspirinP.png|"Хм...Пойдет. Возмжоно, ты не так уж и ${female,безнадежна,безнадежен}."
    #set_flag aspirin_q1_completed,true
    #is_lua getPlayer():HasTrait("Goodmanners")|true
        #reward EXP,RepCrafts,1
        #exit
    #is_lua getPlayer():HasTrait("Badmanners")|true
        #reward EXP,RepCrafts,0.5
        #exit
    #reward EXP,RepCrafts,0.75
    #exit
#m Аспирин|NPCPortraits/AspirinP.png|"Зачем ты тратишь мое время?"
#exit

*questASPIRIN1_2
#is_quest aspirin_q1|unlocked,uncompleted,unfailed
    #m Аспирин|NPCPortraits/AspirinP.png|"Куда ${female,разогналась,разогнался}? По одному заданию за раз!"
    #exit
#is_quest aspirin_q2|locked
    #m Аспирин|NPCPortraits/AspirinP.png|"Че, жажда знаний мучает? Ладно, слушай. С таблетками мы разобрались, теперь перейдем к самому навыку. Многие думают, что бесконечно калечить себя осколками стекла, вытаскивать их из ран и перевязываться - это отличный способ быстрее освоить Первую помощь. Но это чушь... Способ для дураков."
    #m Аспирин|NPCPortraits/AspirinP.png|"Есть куда более действенный метод - разделка трупов. Убиваешь зараженного и с помощью любого острого предмета (хоть камня) аккуратно разделываешь труп. И знания в анатомии подтянешь, и хирургические навыки прокачаешь. А еще есть некоторый шанс получить Образцы тканей зараженного - крайне нужная вещь, так как без нее не создашь ни одного лекарства."
    #m Аспирин|NPCPortraits/AspirinP.png|"Да, забыла сказать. Ты можешь создать любое лекарство. И при создании почти каждого из них тебе нужны будут Образцы тканей. Почему? Долгая история. После Инцидента у всех людей произошли какие-то изменения в организме. Теперь обычные лекарства не работают, если ты не добавишь в них тот самый компонент из крови зараженных."
    #m Аспирин|NPCPortraits/AspirinP.png|"А еще после Инцидента люди стали более сонливыми. Но это не проблема - витамины тоже можно создать. К тому же есть энергетики, чай и все остальное, но я врач, а не кулинар, так что с этим разбирайся без меня. Ты также можешь создавать "аптечки" - такие препараты, которые позволяют немного отсрочить смерть [вливают ХП без исцеления ран]. Это Физраствор, Пакет крови и Шприц с адреналином."
    #m Аспирин|NPCPortraits/AspirinP.png|"У каждого свои условия применения. Шприц, например, лучше не колоть, пока ты не будешь буквально при смерти - иначе рискуешь словить инфаркт. Так же не пренебрегай отварами трав. Штука весьма полезная! И сделай на всякий случай Анализатор крови. Безошибочно определяет, заражен ли ты зомби-вирусом или нет. А еще с его помощью можно проверить соратников! Вдруг кто-то решит скрыть факт заражения..."
    #m Аспирин|NPCPortraits/AspirinP.png|"У местных ученых была оригинальная разработка - Антизин. Это такой препарат, который откатывает прогресс болезни до самого начала. Ну, условно... прогресс болезни 80%, ты принимаешь Антизин - прогресс откатывается до 1%. Позволяет долго жить, пока не добудешь Вакцину. Точнее, ЕСЛИ не добудешь. А твое второе задание - это Образцы зараженного. Иди разделай пару трупов и добудь мне 2 единицы тканей."
    #quest_unlock aspirin_q2
    #task_unlock aspirin_q2|t1 
    #set_flag aspirin_q2_flag,true
    #exit
#is_quest aspirin_q2|uncompleted,unfailed
    #m Аспирин|NPCPortraits/AspirinP.png|"А где образцы?"
    #exit
#m Аспирин|NPCPortraits/AspirinP.png|"Хватит с тебя. Приходи после 9 утра."
#exit

*questASPIRIN1_2_complete
#deliver aspirin_q2|t1
    #m Аспирин|NPCPortraits/AspirinP.png|"Вырезаешь ты пока не очень, но все приходит с опытом. Для лекарств сгодится."
    #set_flag aspirin_q2_completed,true
    #reward Base.Money,1,rare
    #is_lua getPlayer():HasTrait("Goodmanners")|true
        #reward EXP,RepCrafts,1
        #exit
    #is_lua getPlayer():HasTrait("Badmanners")|true
        #reward EXP,RepCrafts,0.5
        #exit
    #reward EXP,RepCrafts,0.75
    #exit
#m Аспирин|NPCPortraits/AspirinP.png|"Режь аккуратнее, иначе испортишь ткани и они станут непригодны к использованию."
#exit

*reset_aspirin_q1
#quest_reset aspirin_q1
#task_reset aspirincooldown|t1
#exit

*reset_aspirin_q2
#quest_reset aspirin_q2
#task_reset aspirincooldown|t2
#exit

*quest_exit
#exit

*questsASPIRINabandon
#m Аспирин|NPCPortraits/AspirinP.png|"Мда... Ну, хотя бы не ${female,скопытилась,скопытился}."
#is_quest aspirin_q1|unlocked,uncompleted,unfailed
    #quest_fail aspirin_q1
    #set_flag aspirin_q1_flag,false
    #task_unlock aspirincooldown|t1 
#is_quest aspirin_q2|unlocked,uncompleted,unfailed
    #quest_fail aspirin_q2
    #set_flag aspirin_q2_flag,false
    #task_unlock aspirincooldown|t2 
#exit

*questsASPIRIN2
#choice
    questASPIRIN1_3|Задание "Очистка воды"
    questASPIRIN1_4|Задание "Волшебная пыльца"
    backtolist|Вернуться к общему списку
    questASPIRIN1_3_complete|Сдать Задание|aspirin_q3_flag
    questASPIRIN1_4_complete|Сдать Задание|aspirin_q4_flag

*questASPIRIN1_3
#is_quest aspirin_q4|unlocked,uncompleted,unfailed
    #m Аспирин|NPCPortraits/AspirinP.png|"Куда ${female,разогналась,разогнался}? По одному заданию за раз!"
    #exit
#is_quest aspirin_q3|locked
    #is_lua getPlayer():getPerkLevel(Perks.RepCrafts) >= 4|true
        #m Аспирин|NPCPortraits/AspirinP.png|"Слушай внимательно. Мы тут недавно обнаружили интересное воздействие вируса на водную живность. И теперь мне регулярно нужны образцы воды из местной реки."
        #m Аспирин|NPCPortraits/AspirinP.png|"Я посылала Авеля в течение пары недель подсыпать в воду один раствор... Теперь нужно понять, как он повлиял на экосреду."
        #m Аспирин|NPCPortraits/AspirinP.png|"Найди колбу, возьми образец воды. Затем нагрей ее где-нибудь - в костре, в печи, на плите. Как пожелаешь. У тебя останется осадок - вот он мне и нужен. И поспеши, важно фиксировать ежедневную разницу между замерами."
        #quest_unlock aspirin_q3
        #task_unlock aspirin_q3|t1 
        #exit
    #m Аспирин|NPCPortraits/AspirinP.png|"Не ${female,доросла,дорос} еще. Набери побольше репутации."
    #exit
#is_quest aspirin_q3|uncompleted,unfailed
    #m Аспирин|NPCPortraits/AspirinP.png|"Ты уже ${female,взяла,взял} это задание. Издеваешься?"
    #exit
#m Аспирин|NPCPortraits/AspirinP.png|"Хватит с тебя. Приходи завтра."
#exit

*quest3_water
#has_item NHM.ChemicalFlask,1
    #remove_item NHM.ChemicalFlask,1
    #m "Просто набрать воды..."
    #add_item NHM.QuestAspirinWater
    #set_flag q3_water_flag,true
    #m Какой странный запах у воды... Надеюсь, в Оазисе знают, что мы ее пьем.
    #exit
#m Мне нужна пустая колба, чтобы добыть образец воды.
#exit

*questASPIRIN1_3_complete
#has_item NHM.QuestAspirinWaterDry,1
    #remove_item NHM.QuestAspirinWaterDry,1
    #add_item NHM.ChemicalFlask,1
    #m Держи. Слушай... А ты же в курсе, что люди пьют воду из реки?
    #m Аспирин|NPCPortraits/AspirinP.png|"Нет, блин, не в курсе. Я ёбнулась и решила всех потравить."
    #m Аспирин|NPCPortraits/AspirinP.png|"Конечно, я в курсе. Мой раствор безвреден, если ты кипятишь воду."
    #m Аспирин|NPCPortraits/AspirinP.png|"Ну, а если ты не кипятишь воду из реки - то земля тебе пухом."
    #m Аспирин|NPCPortraits/AspirinP.png|"Все, спасибо."
    #set_flag aspirin_q3_completed,true
    #reward Base.Money,3,rare
    #is_lua getPlayer():HasTrait("Goodmanners")|true
        #reward EXP,RepCrafts,2
        #exit
    #is_lua getPlayer():HasTrait("Badmanners")|true
        #reward EXP,RepCrafts,1
        #exit
    #reward EXP,RepCrafts,1.5
    #exit
#m Аспирин|NPCPortraits/AspirinP.png|"Что там с образцами воды?"
#exit

*questASPIRIN1_4
#is_quest aspirin_q3|unlocked,uncompleted,unfailed
    #m Аспирин|NPCPortraits/AspirinP.png|"Куда ${female,разогналась,разогнался}? По одному заданию за раз!"
    #exit
#is_quest aspirin_q4|locked
    #is_lua getPlayer():getPerkLevel(Perks.RepCrafts) >= 7|true
        #m Аспирин|NPCPortraits/AspirinP.png|"Так, запоминай. Есть около-секретная разработка от Докторов с Осириса. Название они еще не придумали, зато я придумала! Пыльца. Как тебе?"
        #m Аспирин|NPCPortraits/AspirinP.png|"Ладно, мне все равно. Пыльца - это смесь двух трав. Аконит и Рута. Если смешать их в ступке вместе с Образцами зараженного, то получится Пыльца."
        #m Аспирин|NPCPortraits/AspirinP.png|"Проблема в том, что Аконит и Рута растут в разных местах. Один возле трейлерного городка неподалеку отсюда, второй - неподалеку от заправки Вест-Поинта. Прямо на дороге. Примерные координаты я тебе запишу. Да и фото растений дам, чтоб не ${female,перепутала,перепутал} ничего."
        #m Аспирин|NPCPortraits/AspirinP.png|"Ну и про образцы зараженного не забудь. Придется немного испачкать руки."
        #m Аспирин|NPCPortraits/AspirinP.png|"План очень прост. Приходишь сначала за Аконитом, затем за Рутой, затем убиваешь парочку зомби ради Образцов зараженного. Затем все полученное объединяешь в ступе с пестиком."
        #m Аспирин|NPCPortraits/AspirinP.png|"Чуть не забыла... Для самого процесса объединения тебе понадобится Верстак Медика. Вроде всё. Удачи."
        #quest_unlock aspirin_q4
        #task_unlock aspirin_q4|t1 
        #exit
    #m Аспирин|NPCPortraits/AspirinP.png|"Куда ${female,разогналась,разогнался}? Не по зубам тебе такие задания."
    #exit
#is_quest aspirin_q4|uncompleted,unfailed
    #m Аспирин|NPCPortraits/AspirinP.png|"Ты уже ${female,взяла,взял} это задание. Издеваешься?"
    #exit
#m Аспирин|NPCPortraits/AspirinP.png|"Хватит с тебя. Приходи завтра."
#exit

*quest4_akon
    #m "Должен быть где-то здесь... Кажется, это он!"
    #add_item NHM.QuestHerbOne
    #set_flag q4_akon_flag,true
    #m Довольно резкий запах... Пора выдвигаться за Рутой.
#exit

*quest4_rutah
    #m "Биология в этой местности претерпела изменения. Растения растут прямо на дороге..."
    #add_item NHM.QuestHerbTwo
    #set_flag q4_rutah_flag,true
    #m Что Аконит, что Рута... Немного отличаются от тех, что на фото. Полагаю, это воздействие зомби-вируса. 
    #m Ладно, не время. Пора добыть Образцы зараженного. 
#exit

*questASPIRIN1_4_complete
#has_item NHM.QuestHerbMix,1
    #remove_item NHM.QuestHerbMix,1
    #m Вот твоя Пыльца. А в чем именно ее эффект?
    #m Аспирин|NPCPortraits/AspirinP.png|"Губу закатай. Так я тебе и рассказала. ${female,Сама,Сам} наведайся в Осирис, ну, или в Лабораторию Прометея. Может, там и расскажут."
    #m Ну ладно...
    #m Аспирин|NPCPortraits/AspirinP.png|"На этом всё, спасибо."
    #set_flag aspirin_q4_completed,true
    #reward Base.Money,3,rare
    #is_lua getPlayer():HasTrait("Goodmanners")|true
        #reward EXP,RepCrafts,4
        #exit
    #is_lua getPlayer():HasTrait("Badmanners")|true
        #reward EXP,RepCrafts,2
        #exit
    #reward EXP,RepCrafts,3
    #exit
#m Аспирин|NPCPortraits/AspirinP.png|"Где Пыльца?"
#exit

*reset_aspirin_q3
    #set_flag q3_water_flag,false
    #set_flag aspirin_q3_flag,false
    #task_complete aspirin_q3|t0
#exit

*reset_aspirin_q4
    #set_flag q4_akon_flag,false
    #set_flag q4_rutah_flag,false
    #set_flag aspirin_q4_flag,false
    #task_complete aspirin_q4|t0
#exit

