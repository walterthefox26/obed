#is_flag dark_intro,true
    #jump loop

*intro
#m Дарк|NPCPortraits/DarkP.png|"Тебя как занесло сюда?"

#choice
    intro_hello|Познакомиться
    intro_leave|Уйти

*intro_leave
#m Дарк|NPCPortraits/DarkP.png|"Ну... ладно."
#exit

*intro_hello
#m "Здравствуйте."
#m Дарк|NPCPortraits/DarkP.png|"Что имеет 2 ноги и истекает кровью?"
#m "Эээ... Я не знаю."
#m Дарк|NPCPortraits/DarkP.png|"Половина собаки!"
#m Картер|NPCPortraits/CarterP.png|"Как же ты заебал."
#m Картер|NPCPortraits/CarterP.png|"Отъебись от людей со своими шутками."
#m Дарк|NPCPortraits/DarkP.png|"Есть, сэр! Чарльз Дарк, капрал Армии США."
#m Дарк|NPCPortraits/DarkP.png|"Это капитан Картер, а там, чуть подальше, полковник Маршалл."
#m Дарк|NPCPortraits/DarkP.png|"Желтое и не умеет плавать. Что это?"
#m "Что?"
#m Дарк|NPCPortraits/DarkP.png|"Автобус с детьми!"
#m Картер|NPCPortraits/CarterP.png|"Да ёб твою мать."
#m Дарк|NPCPortraits/DarkP.png|"Виноват, капитан Картер! Эй, салага, ты уж не обижайся. Новых лиц тут почти не бывает, так что эти шутки копятся неделями!"
#m Дарк|NPCPortraits/DarkP.png|"Как известно, в мире не стареют только две вещи. Черный юмор и невакцинированные дети."
#m Женщина бросает злобный взгляд на Дарка. Тот приходит в себя и обращается к вам.
#m Дарк|NPCPortraits/DarkP.png|"Ты можешь выполнить поручения для меня. Сложные и опасные, но и наградой не обижаем. Заходи, в общем."
#fade_out black
#set_flag dark_intro,true
#fade_in
#reveal Dark
#exit

*loop
#m Дарк|NPCPortraits/DarkP.png|"Если бы моя бабушка знала, сколько денег я сэкономил на ее похоронах, она бы перевернулась в канаве."

#is_quest dark_q1|locked
    #set_flag dark_q1_flag,false
#is_quest dark_q1|completed
    #set_flag dark_q1_flag,false
#is_quest dark_q1|failed
    #set_flag dark_q1_flag,false

#is_quest dark_q2|locked
    #set_flag dark_q2_flag,false
#is_quest dark_q2|completed
    #set_flag dark_q2_flag,false
#is_quest dark_q2|failed
    #set_flag dark_q2_flag,false
    
*backtolist
#choice
    questBunker|Задания Бункера
    quest_exit|Уйти

*questZombies
#choice
    questZ1|Задание "Утопия"
    questZ2|Задание "Безногий Паук"
    backtolist|Вернуться к общему списку
    questZ1_complete|Завершить задание|dark_q1_flag
    questZ2_complete|Завершить задание|dark_q2_flag

*questZ1
#is_quest dark_q2|unlocked,uncompleted,unfailed
    #m Дарк|NPCPortraits/DarkP.png|"Не, ${female,подруга,друг}. Не переоценивай себя. Не больше одного задания."
    #exit
#is_quest dark_q1|locked
    #m Дарк|NPCPortraits/DarkP.png|"Однажды мой друг потерялся на минном поле. Знаешь, где мы его нашли?" 
    #m "Где?.."
    #m Дарк|NPCPortraits/DarkP.png|"Везде." 
    #m Дарк пристально смотрит вам в глаза.
    #m Дарк|NPCPortraits/DarkP.png|"В общем, слушай. Мне нужно, чтоб ты ${female,пробралась,пробрался} в Бункер и ${female,добыла,добыл} мне там кое-что. Не знаю, ${female,общалась,общался} ли ты с Авелем, но, возможно, ты и так уже ${female,догадалась,догадался}, что у нас есть некоторые проблемы с провизией." 
    #m Дарк|NPCPortraits/DarkP.png|"Не хватает рук добывать пищу, Ремесленники нас не любят, а у Докторов есть дела поважнее. Но вообще-то... есть одно решение. В Бункере еще до Инцидента проводили исследования. И вывели сорт устойчивой культуры. Крайне живучая и крайне плодовитая. Растет быстро, требует мало."
    #m Дарк|NPCPortraits/DarkP.png|"Проблема в том, что семена не работают. Точнее... они не успели завершить разработку. В итоге сейчас это просто нестабильная хреновина, умирающая без нужной температуры. В Бункере еще много запасов этого добра."  
    #m Дарк|NPCPortraits/DarkP.png|"А вот здесь у нас, к сожалению, нужных условий нет. Поэтому мы используем единственный доступный нам вариант - мотаемся до Бункера и приносим образцы поштучно. Приносить больше смысла нет - Доктора не успевают обработать больше одного образца, прежде чем они погибают." 
    #m Дарк|NPCPortraits/DarkP.png|"Поэтому вот твое задание. Отправляйся в Бункер. Найди столовую. Найди Семена Утопии (да, они и правда так называются). А затем за очень короткий промежуток времени привези их сюда. При недостаточно низкой температуре они умрут за пару часов."
    #m Дарк|NPCPortraits/DarkP.png|"Кстати, знаешь почему моему отцу нельзя посещать вечеринки?"
    #m "Ну... почему?"
    #m Дарк|NPCPortraits/DarkP.png|"Потому что он умер." 
    #m "Я, пожалуй, пойду."
    #quest_unlock dark_q1
    #task_unlock dark_q1|t1
    #exit
#is_quest dark_q1|uncompleted,unfailed
    #m Дарк|NPCPortraits/DarkP.png|"Черный юмор - как еда. Доступен не всем. А задание ты уже выполняешь."
    #exit
#m Дарк|NPCPortraits/DarkP.png|"Возвращайся завтра."
#exit

*q1_choosecont
#roll con1,10|con2,20|con3,30|con4,40|con5,50|con6,60|con7,70|con8,80|con9,90|con10,100
#exit

*con1
#task_unlock dark_q1|t4
#exit

*con2
#task_unlock dark_q1|t5
#exit

*con3
#task_unlock dark_q1|t6
#exit

*con4
#task_unlock dark_q1|t7
#exit

*con5
#task_unlock dark_q1|t8
#exit

*con6
#task_unlock dark_q1|t9
#exit

*con7
#task_unlock dark_q1|t10
#exit

*con8
#task_unlock dark_q1|t11
#exit

*con9
#task_unlock dark_q1|t12
#exit

*con10
#task_unlock dark_q1|t13
#exit

*questZ1_complete
#task_complete dark_q1|t15
#m Дарк|NPCPortraits/DarkP.png|"Однажды мы с дочерью смотрели на звездное небо. И она спросила меня: Пап, а как умирают звезды?"
#m Дарк|NPCPortraits/DarkP.png|"Обычно от передоза - ответил я."
#m Дарк|NPCPortraits/DarkP.png|"Ладно, шучу. Нет у меня дочери. А за Семена спасибо."
#m "Ты и правда ёбнутый."
#set_flag dark_q1_completed,true
#reward Base.Money,5,rare
#is_lua getPlayer():HasTrait("Goodmanners")|true
    #reward EXP,RepArmy,3
    #exit
#is_lua getPlayer():HasTrait("Badmanners")|true
    #reward EXP,RepArmy,1.5
    #exit
#reward EXP,RepArmy,2.25
#exit

*fail_dark_q1
#is_quest dark_q1|unlocked,uncompleted,unfailed
    #set_flag dark_q1_flag,false
    #quest_fail dark_q1
    #exit
#exit

*quest_exit
#exit

*questZ2
#is_quest dark_q1|unlocked,uncompleted,unfailed
    #m Дарк|NPCPortraits/DarkP.png|"Не, ${female,подруга,друг}. Не переоценивай себя. Не больше одного задания."
    #exit
#is_quest dark_q2|locked
    #is_lua getPlayer():getPerkLevel(Perks.RepArmy) >= 4|true
        #has_item Radio.WalkieTalkie4,1
            #m Дарк|NPCPortraits/DarkP.png|"Знаешь, в чем разница между маленькой девочкой и тараканами?" 
            #m Дарк|NPCPortraits/DarkP.png|"У меня в подвале нет тараканов."
            #m Дарк|NPCPortraits/DarkP.png|"Ладно, в общем... Есть у меня парочка посыльных. Обмениваемся с ними важной информацией. И ресурсами иногда. Они живут под Луисвиллем и периодически бегают в город за припасами."
            #m Дарк|NPCPortraits/DarkP.png|"Как ты, наверно, уже знаешь - Луисвилль заражен. И зомби там многовато... Короче, мой посыльный перестал выходить на связь. И тебе надо выяснить, что случилось."
            #m Дарк|NPCPortraits/DarkP.png|"Его зовут Паук. Я дам тебе пару мест, где он может быть. Проверь эти места, найди его следы, а еще лучше - его самого. Будем держать связь по рации. Можешь идти."
            #m Дарк|NPCPortraits/DarkP.png|"А теперь вопрос. Как назвать безногую собаку?"
            #m "Да сколько можно... Ну и как же?"
            #m Дарк|NPCPortraits/DarkP.png|"Неважно. Она все равно не прибежит."
            #m "Какой же ты придурок."
            #quest_unlock dark_q2
            #task_unlock dark_q2|t1
            #exit
        #m Дарк|NPCPortraits/DarkP.png|"Для этого задания тебе нужна Портативная тактическая рация."
        #exit
    #m Дарк|NPCPortraits/DarkP.png|"Ты, конечно, не карлик, но до этого задания еще не дорос."
    #exit
#is_quest dark_q2|uncompleted,unfailed
    #m Дарк|NPCPortraits/DarkP.png|"Ты же уже ${female,брала,брал} задание. Выполни для начала."
    #exit
#m Дарк|NPCPortraits/DarkP.png|"Навсегда запомню последние слова моего деда. "Дебил ёбаный, держи лестницу!". Каждый раз до слез... А заданий у меня для тебя пока нет. Приходи завтра."
#exit

*q2_choosepath
#roll path1,33|path2,66|path3,100
#exit

*path1
#task_unlock dark_q2|t2
#exit

*path2
#task_unlock dark_q2|t12
#exit

*path3
#task_unlock dark_q2|t15
#exit

*quest2_path1
#has_item Radio.WalkieTalkie4,1
    #m "Эй, Дарк. Я тут ${female,прибыла,прибыл} в то место, которое ты указал. И тут... ничего." 
    #m Дарк|NPCPortraits/DarkP.png|"Что, совсем? Погоди, это же Паук. Он не мог не оставить записку. Посмотри внимательно."
    #m "Кажется, ты прав. Я что-то ${female,нашла,нашел}. Записка. Вот только..."
    #m Дарк|NPCPortraits/DarkP.png|"Не понимаешь, что написано? И не пытайся. Текст зашифрован. Мы же Военные, в конце концов."
    #m "Эээ... Нет. Вообще-то, я все понимаю. Здесь написано "меня подбили, попробую добраться до церкви, ищи меня там". Он знал, что его будут искать?"
    #m Дарк|NPCPortraits/DarkP.png|"Конечно, знал. Я своих информаторов в беде не оставляю. Но в церкви его не будет. Найди данные. Узнаешь сразу. Ты не сможешь их прочитать - они зашифрованы, как я уже говорил."
    #m "А с чего ты взял, что его там не будет?"
    #m Дарк|NPCPortraits/DarkP.png|"Просто поверь мне. Выдвигайся."
    #set_flag q2_p1_flag,true
    #exit
#m Дарк|NPCPortraits/DarkP.png|"Не могу связаться с Дарком без рации."
#exit

*q2_choosecont
#roll c1,20|c2,40|c3,60|c4,80|c5,100
#exit

*c1
#task_unlock dark_q2|t6
#exit

*c2
#task_unlock dark_q2|t7
#exit

*c3
#task_unlock dark_q2|t8
#exit

*c4
#task_unlock dark_q2|t9
#exit

*c5
#task_unlock dark_q2|t10
#exit

*quest2_path2
#has_item Base.Crowbar,1|Radio.WalkieTalkie4,1
    #m "Эй, Дарк. Я тут ${female,прибыла,прибыл} в то место, которое ты указал. Вижу здесь рельсы. И, кажется, их недавно разбирали." 
    #m Дарк|NPCPortraits/DarkP.png|"Монтировка у тебя с собой, надеюсь? Вскрывай. Это в духе Паука."
    #m "С собой. Сейчас попробую..."
    #add_item NHM.QuestSpiderNote,1
    #m "Есть!"
    #m Дарк|NPCPortraits/DarkP.png|"Дуй обратно."
    #set_flag q2_p2_flag,true
    #exit
#m Дарк|NPCPortraits/DarkP.png|"Не могу связаться с Дарком без рации. А еще мне, кажется, нужна монтировка."
#exit

*quest2_path3
#has_item Radio.WalkieTalkie4,1
    #m "Прием, как слышно?" 
    #m Паук|NPCPortraits/SpiderP.png|"А ты еще кто?"
    #m "Стоп... Паук?"
    #m Паук|NPCPortraits/SpiderP.png|"Ага. Тебя Дарк прислал?"
    #m "Да. Говорит, ты на связь выходить перестал."
    #m Паук|NPCPortraits/SpiderP.png|"Меня мертвые зажали и я потерял рацию. Портативную тактическую так просто не найдешь. Нашел обычную - вот с тобой связался."
    #m Паук|NPCPortraits/SpiderP.png|"Я в соседнем здании. Заперся на крыше. Увидел, как ты заходишь."
    #m "Дарк говорил, что ты должен ему что-то передать."
    #m Паук|NPCPortraits/SpiderP.png|"Приходи и зачисти первый этаж. Я скину тебе послание, передашь Дарку."
    #m "А помощь тебе не нужна?"
    #m Паук|NPCPortraits/SpiderP.png|"Нет. Зачисти и уебывай. Дальше я сам."
    #set_flag q2_p3_flag,true
    #exit
#m Дарк|NPCPortraits/DarkP.png|"Не могу связаться с Дарком без рации."
#exit

*quest2_note    
    #m Паук|NPCPortraits/SpiderP.png|"Эй, я тут, наверху. Лови."
    #m "Что, даже не спустишься?"
    #m Паук|NPCPortraits/SpiderP.png|"Я ранен. Подлатаюсь и спущусь, через пару часов. Надо передохнуть."
    #m Паук|NPCPortraits/SpiderP.png|"Записку не читай, все равно не поймешь. Передай Дарку и всё. Спасибо за помощь."
    #add_item NHM.QuestSpiderNote,1
    #set_flag q2_note_flag,true
#exit

*questZ2_complete
#m Дарк|NPCPortraits/DarkP.png|"В детстве я плакал, когда мой отец резал лук. Лук был хорошим псом."
#m "Почему столько шуток про собак..."
#m Дарк|NPCPortraits/DarkP.png|"Эээ... не знаю. Ты молодец, спасибо. Держи награду."
#set_flag dark_q2_completed,true
#reward Base.Money,5,rare
#is_lua getPlayer():HasTrait("Goodmanners")|true
    #reward EXP,RepArmy,6
    #exit
#is_lua getPlayer():HasTrait("Badmanners")|true
    #reward EXP,RepArmy,3
    #exit
#reward EXP,RepArmy,4.5
#exit

*reset_dark_q1
    #set_flag dark_q1_flag,false
    #task_complete dark_q1|t0
#exit

*reset_dark_q2
    #set_flag dark_q2_flag,false
    #set_flag q2_p1_flag,false
    #set_flag q2_p2_flag,false
    #set_flag q2_p3_flag,false
    #set_flag q2_note_flag,false
    #task_complete dark_q2|t0
#exit