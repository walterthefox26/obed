#quest cleaner_q1
    #task t1
        #set GotoLocation|11540,7563,11543,7567,1|true
        #action UnlockTask|cleaner_q1|t2
    #task t2
        #set ContextAction|Отмыть красное пятно|11540,7565,1|2|Cleaner.txt,quest1_q1_clean|q1_clean_flag
        #action UnlockTask|cleaner_q1|t3
        #action SetFlag|cleaner_q1_flag|true
        #action SetFlag|q1_clean_flag|false
    #task t3
        #set RaiseFlag|cleaner_q1_completed
        #action Reward|Base.Money,1,common
        #action Reward|EXP,RepDoc,3
        #action SetFlag|cleaner_q1_completed|false
        #action SetFlag|cleaner_q1_flag|false
        #action CompleteQuest|cleaner_q1
