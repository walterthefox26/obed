#quest assistant_q1
    #task t1
        #set GotoLocation|11554,7548,11556,7552,1|true
        #action UnlockTask|assistant_q1|t2
    #task t2
        #set ContextAction|Посмотреть в телескоп|11553,7543,1|2|Assistant.txt,quest1_q1_assistant|q1_assistant_flag
        #action UnlockTask|assistant_q1|t3
        #action SetFlag|assistant_q1_flag|true
        #action SetFlag|q1_assistant_flag|false
    #task t3
        #set RaiseFlag|assistant_q1_completed
        #action Reward|Base.Money,1,common
        #action Reward|EXP,RepDoc,3
        #action SetFlag|assistant_q1_completed|false
        #action SetFlag|assistant_q1_flag|false
        #action CompleteQuest|assistant_q1